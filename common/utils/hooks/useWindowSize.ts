import { useEffect, useState } from "react";

function debounce<T extends (...args: any[]) => void>(
  callback: T,
  delay: number
) {
  let timeout: ReturnType<typeof setTimeout> | null;

  return function <U>(this: U, ...args: Parameters<typeof callback>) {
    timeout = setTimeout(() => {
      timeout = null;
      callback.apply(this, args);
    }, delay);
  };
}

const useWindowSize = () => {
  const [windowDimention, setWindowDimention] = useState({
    width: 0,
    height: 0,
  });

  const handleWindowResize = () => {
    setWindowDimention({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  };

  const debouncedWindowResize = debounce(handleWindowResize, 500);

  useEffect(() => {
    handleWindowResize();
    window.addEventListener("resize", debouncedWindowResize);
    return () => window.removeEventListener("resize", debouncedWindowResize);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return { width: windowDimention.width, height: windowDimention.height };
};

export default useWindowSize;
