export const _NAVBAR_ = [
  {
    menu: "About",
    href: "/about",
    target: "",
  },
  {
    menu: "Services",
    href: "",
    target: "popover",
  },
  // {
  //   menu: "Blog",
  //   href: "/blog",
  //   target: "",
  // },
  {
    menu: "Contact",
    href: "/contact-us",
    target: "",
  },
  {
    menu: "Careers",
    href: "https://www.kalibrr.com/id-ID/c/hiaria/jobs",
    target: "_blank",
  },
];

export const _SUCCSESS_STORIES_ = [
  {
    title: "How Russian - Ukraine War Effected to Food Security?",
    excerpt:
      "Russian - Ukraine war has effected to food chain around the world. A lot of country feel the food crisis. How it can be solve?",
    date: "Mon, 12 December 2022",
    name: "By Jane Doe",
    category: "Business",
  },
  {
    title: "10 Tips for Pests and Diseases Management in Agriculture",
    excerpt:
      "A lot of mistakes that we’ve made while farming which leads to crop failure. How we can prevent any crop failure?",
    date: "Mon, 12 December 2022",
    name: "By Jane Doe",
    category: "Lifestyle",
  },
  {
    title: "Challenges of Modern Agriculture, is It Complex?",
    excerpt:
      "Modern agriculture has many complex challenges for farmers that hard for them to survive. ",
    date: "Mon, 12 December 2022",
    name: "By Jane Doe",
    category: "Business",
  },
];

export const _ADDRESS_CONTENT_ = [
  {
    image: "/icons/footer/location.svg",
    alt: "location-logo",
    desc: "JL Sunter Agung Podomoro Blok N2, No.9-10, Sunter,RT.10/RW.11, Sunter Jaya, Tj. Priok, Kota Jakarta Utara, Daerah Khusus Ibukota Jakarta, 14350",
  },
  {
    image: "/icons/footer/mail.svg",
    alt: "mail-logo",
    desc: "marketing@hiaria.id",
  },
  {
    image: "/icons/footer/call.svg",
    alt: "call-logo",
    desc: "+62 812-8383-2031",
  },
];

export const _SOSMED_ITEMS_ = [
  {
    logo: "/icons/footer/linkedin.svg",
    alt: "linkedind-logo",
    href: "https://www.linkedin.com/company/hiaria/?originalSubdomain=id",
  },
  {
    logo: "/icons/footer/facebook.svg",
    alt: "facebook-logo",
    href: "https://www.facebook.com/ariaagriculture",
  },
  {
    logo: "/icons/footer/youtube.svg",
    alt: "youtube-logo",
    href: "https://www.youtube.com/@ariaagriculture",
  },
  {
    logo: "/icons/footer/instagram.svg",
    alt: "instagram-logo",
    href: "https://www.instagram.com/ariaindonesia.id/",
  },
];
