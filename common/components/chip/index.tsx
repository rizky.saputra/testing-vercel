const Chip = ({ title }: any) => {
  return (
    <div className="absolute top-3 left-2">
      <span className="px-3 py-2 rounded-full text-[10px] text-white font-semibold sm:font-normal sm:text-[12px] bg-[#1B974E]">
        {title}
      </span>
    </div>
  );
};

export default Chip;
