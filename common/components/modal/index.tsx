import { useEffect, useRef } from "react";

function Modal({ onClickOutside, open, children, title }: any) {
  const ref: any = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (ref.current && !ref.current.contains(event.target)) {
        onClickOutside && onClickOutside();
      }
    };
    document.addEventListener("click", handleClickOutside, true);
    return () => {
      document.removeEventListener("click", handleClickOutside, true);
    };
  }, [onClickOutside]);

  return (
    <>
      {open && (
        <div>
          <div
            className={`justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none`}
          >
            <div
              className="relative w-auto my-6 mx-auto min-w-[400px]"
              ref={ref}
            >
              <div className="border-0 rounded-[16px] shadow-lg relative flex flex-col w-full outline-none focus:outline-none bg-[#F6F6F6]">
                {title && (
                  <div className="flex items-start justify-between p-5 border-b border-dashed border-slate-200 rounded-t">
                    <p className="text-[20px] font-semibold">{title}</p>
                    <button onClick={() => onClickOutside()}>
                      <span className="material-icons-outlined text-[#DADADA] text-[18px] align-middle">
                        X
                      </span>
                    </button>
                  </div>
                )}

                {children}
              </div>
            </div>
          </div>
          <div className={`opacity-25 fixed inset-0 z-40 bg-black`}></div>
        </div>
      )}
    </>
  );
}

export default Modal;
