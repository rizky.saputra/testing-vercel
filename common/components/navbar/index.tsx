/** @format */

import Image from "next/image";
import Link from "next/link";
import { _NAVBAR_ } from "common/constant";
import { imageLoader } from "common/utils";
import { useEffect, useRef, useState } from "react";
import { useRouter } from "next/router";

const Navbar = () => {
  const [open, setOpen] = useState(false);

  return (
    <nav className="bg-[#255855] py-2.5 sticky top-0 z-20 flex w-full">
      <MobileMenu openState={[open, setOpen]} />
      <div className="flex flex-grow justify-center sm:justify-between pl-[35px] pr-[100px] sm:pl-[100px] [@media(min-width:1700px)]:px-[300px] [@media(min-width:2000px)]:px-[400px]">
        {!open && (
          <div className="flex justify-start">
            <Link href={"/"}>
              <Image
                loader={imageLoader}
                alt="aria-logo"
                src={"/logo-asset/aria-logo-text.png"}
                width={150}
                height={0}
                className="h-auto w-[100px] sm:w-[150px]"
                priority
              />
            </Link>
          </div>
        )}

        <div className="hidden w-full md:block md:w-auto">
          <ul className="sm:flex flex-col p-4 mt-4 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0 text">
            {_NAVBAR_.map((item, index) => {
              return (
                <div key={index}>
                  {item.target === "popover" ? (
                    <MenuPopOver {...item} />
                  ) : (
                    <li key={index}>
                      <Link
                        href={item.href}
                        target={item.target}
                        className="block py-2 pl-3 pr-4 text-white hover:text-[#07a9a0] font-light text-[16px]"
                      >
                        {item.menu}
                      </Link>
                    </li>
                  )}
                </div>
              );
            })}
          </ul>
        </div>
      </div>
      {open && (
        <button
          id="close-button"
          className="m-2 mr-5 w-[30px] h-[30px]"
          onClick={() => setOpen(!open)}
        >
          <svg
            fill="none"
            stroke="white"
            strokeWidth="1.5"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
            aria-hidden="true"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M6 18L18 6M6 6l12 12"
            ></path>
          </svg>
        </button>
      )}
    </nav>
  );
};

export default Navbar;

const _MENU_ITEM_ = [
  {
    title: "Area Mapping",
    desc: "Aerial photography and analysis",
    icon: "/icons/navbar/area-mapping-agri.png",
    href: "/aria-agriculture/area-mapping",
  },
  {
    title: "Drone Spraying",
    desc: "Automated blanket and spot spraying",
    icon: "/icons/navbar/drone-spraying-agri.png",
    href: "/aria-agriculture/drone-spraying",
  },
  {
    title: "Data Platform",
    desc: "Data, analysis, and visualization",
    icon: "/icons/navbar/data-platform-agri.png",
    href: "/aria-agriculture/data-platform",
  },
  {
    title: "Digital Agronomist",
    desc: "Measure and predict nutrient levels",
    icon: "/icons/navbar/digital-agronomist-agri.png",
    href: "/aria-agriculture/digital-agronomist",
  },
  {
    title: "Turbo Spreader",
    desc: "Mechanical fertilizing with usage detection",
    icon: "/icons/navbar/turbo-spreader-agri.png",
    href: "/aria-agriculture/turbo-spreader",
  },
  {
    title: "Worker Tracker",
    desc: "Track movements and area coverage",
    icon: "/icons/navbar/worker-tracker-agri.png",
    href: "/aria-agriculture/worker-tracker",
  },
];

const MobileMenu = ({ openState }: any) => {
  const [open, setOpen] = openState;
  const ref: any = useRef(null);
  const [menu, setMenu] = useState(0);

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (ref.current && !ref.current.contains(event.target)) {
        if (event.target.id !== "servicebutton") {
          setOpen(false);
          setMenu(0);
        }
      }
    };
    document.addEventListener("click", handleClickOutside, true);
    return () => {
      document.removeEventListener("click", handleClickOutside, true);
    };
  }, []);

  const router = useRouter();

  return (
    <div className="relative sm:hidden">
      {!open ? (
        <button
          id="servicebutton"
          aria-label="menu button"
          className="m-2 ml-5 w-[30px] h-[30px]"
          onClick={() => setOpen(!open)}
        >
          <svg
            fill="none"
            stroke="white"
            strokeWidth="1.5"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
            aria-hidden="true"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
            ></path>
          </svg>
        </button>
      ) : (
        <Link href={"/"}>
          <Image
            loader={imageLoader}
            alt="aria-logo"
            src={"/logo-asset/aria-logo.png"}
            width={48}
            height={0}
            className="ml-5 h-auto w-[48px]"
            priority
          />
        </Link>
      )}

      <div
        className={`${
          open ? "opacity-100 h-max" : "opacity-0 h-[0]"
        } absolute ${
          menu === 0 ? "top-[55px]" : "top-[-10px]"
        } w-screen bg-[#255855] overflow-hidden`}
        ref={ref}
      >
        {menu === 0 && (
          <>
            {_NAVBAR_.map((item, index) => {
              return (
                <button
                  type="button"
                  onClick={() => {
                    if (item.target === "popover") {
                      setMenu(1);
                    } else {
                      setOpen(false);
                      router.push(item.href);
                    }
                  }}
                  className="flex w-full justify-between border-t-[1px] text-[12px] text-[#F6F6F6] px-7 py-4"
                  key={index}
                >
                  <p>{item.menu}</p>
                  <div className="w-[15px]">
                    <svg
                      fill="none"
                      stroke="currentColor"
                      strokeWidth="1.5"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                      aria-hidden="true"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M8.25 4.5l7.5 7.5-7.5 7.5"
                      ></path>
                    </svg>
                  </div>
                </button>
              );
            })}
          </>
        )}
        {menu === 1 && (
          <div className="flex px-3 pb-5">
            <div className="grow mr-4">
              <div className="p-2 pt-4 pl-1 mb-1 border-b-[0.56px] border-dashed">
                <Link
                  onClick={() => setOpen(!open)}
                  href={"/aria-agriculture"}
                  className="text-[12px]  text-[#F6F6F6] hover:text-[#0DAF4F]"
                >
                  Aria Agriculture
                </Link>
              </div>
              {_MENU_ITEM_.map((item: any, index: any) => (
                <div key={index}>
                  <MobileMenuItem {...item} action={[open, setOpen]} />
                </div>
              ))}
            </div>
            <div className="grow">
              <div className="p-2 pt-4 pl-1 border-b-[0.56px] border-dashed">
                <Link
                  onClick={() => setOpen(!open)}
                  href={"/aria-tani"}
                  className="text-[12px]  text-[#F6F6F6] hover:text-[#0DAF4F]"
                >
                  Aria Tani
                </Link>
              </div>
              <div className="p-2 pl-1 pr-0 border-b-[0.56px] border-dashed">
                <p className="text-[12px] text-[#F6F6F6]">
                  Aria Mining
                  <span className="text-[5px] text-white whitespace-nowrap bg-[#EA9000] p-[4px] ml-2 rounded-[4px] keep-all">
                    coming soon
                  </span>
                </p>
              </div>
              <div className="p-2 pl-1 pr-0 border-b-[0.56px] border-dashed">
                <p className="text-[12px]  text-[#F6F6F6]">
                  Aria Academy
                  <span className="text-[5px] text-white whitespace-nowrap bg-[#7B1F9E] p-[4px] ml-2 rounded-[4px]">
                    coming soon
                  </span>
                </p>
              </div>
            </div>
            <div className="pt-4 pl-1">
              <button
                id="close-button"
                className="w-[30px] h-[30px]"
                onClick={() => setOpen(!open)}
              >
                <svg
                  fill="none"
                  stroke="white"
                  strokeWidth="1.5"
                  viewBox="0 0 24 24"
                  xmlns="http://www.w3.org/2000/svg"
                  aria-hidden="true"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M6 18L18 6M6 6l12 12"
                  ></path>
                </svg>
              </button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

const MenuPopOver = ({ menu }: any) => {
  const [open, setOpen] = useState(false);
  const ref: any = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (ref.current && !ref.current.contains(event.target)) {
        if (event.target.id !== "servicebutton") {
          setOpen(false);
        }
      }
    };
    document.addEventListener("click", handleClickOutside, true);
    return () => {
      document.removeEventListener("click", handleClickOutside, true);
    };
  }, []);

  return (
    <div className="relative">
      <button
        aria-label="servicebutton"
        id="servicebutton"
        onClick={() => setOpen(!open)}
        className="block py-2 pl-3 pr-4 text-white hover:text-[#07a9a0] font-light text-[16px]"
      >
        {menu}
      </button>
      <div
        className={`${
          open ? "opacity-100 h-[480px]" : "opacity-0 h-[0]"
        } absolute p-2 px-4 top-[55px] left-[-500px] w-[733px] bg-white rounded-[12px] transition-all duration-500 ease-in-out overflow-hidden`}
        ref={ref}
      >
        <div className="flex">
          <div className="w-full mr-4">
            <div className="p-4 pl-1 border-b-2 border-dashed">
              <Link
                onClick={() => setOpen(!open)}
                href={"/aria-agriculture"}
                className="text-base text-[#222222] hover:text-[#0DAF4F]"
              >
                Aria Agriculture
              </Link>
            </div>
            {_MENU_ITEM_.map((item: any, index: any) => (
              <div key={index}>
                <MenuItem {...item} action={[open, setOpen]} />
              </div>
            ))}
          </div>
          <div className="w-full max-w-[300px]">
            <div className="p-4 pl-1 border-b-2 border-dashed">
              <Link
                onClick={() => setOpen(!open)}
                href={"/aria-tani"}
                className="text-[16px] text-[#222222] hover:text-[#0DAF4F]"
              >
                Aria Tani
              </Link>
            </div>
            <div className="p-4 pl-1 border-b-2 border-dashed">
              <p className="text-base text-[#222222]">
                Aria Mining
                <span className="text-[8px] text-white bg-[#EA9000] p-[4px] ml-3 rounded-[4px]">
                  coming soon
                </span>
              </p>
            </div>
            <div className="p-4 pl-1 border-b-2 border-dashed">
              <p className="text-base text-[#222222]">
                Aria Academy{" "}
                <span className="text-[8px] text-white bg-[#7B1F9E] p-[4px] ml-3 rounded-[4px]">
                  coming soon
                </span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const MobileMenuItem = ({ title, desc, action, href, icon }: any) => {
  const [open, setOpen] = action;
  return (
    <div className="pt-1 flex items-center">
      <div className="mr-2">
        <Image
          loader={imageLoader}
          alt={"item.title"}
          src={icon}
          width={40}
          height={40}
          priority
        />
      </div>
      <div>
        <Link
          onClick={() => setOpen(!open)}
          href={href}
          className="text-[12px] text-[#F6F6F6] hover:text-[#07A9A0] font-semibold"
        >
          {title}
        </Link>
      </div>
    </div>
  );
};

const MenuItem = ({ title, desc, action, href, icon }: any) => {
  const [open, setOpen] = action;
  return (
    <div className="pt-5 pl-1 flex items-center">
      <div className="mr-4">
        <Image
          loader={imageLoader}
          alt={"item.title"}
          src={icon}
          width={40}
          height={40}
          priority
        />
      </div>
      <div>
        <Link
          onClick={() => setOpen(!open)}
          href={href}
          className="text-base text-[#404040] hover:text-[#07A9A0] font-bold"
        >
          {title}
        </Link>
        <p className="text-sm text-[#808080] font-normal">{desc}</p>
      </div>
    </div>
  );
};
