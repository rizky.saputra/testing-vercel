/** @format */

import { _ADDRESS_CONTENT_, _SOSMED_ITEMS_ } from "common/constant";
import { imageLoader } from "common/utils";
import Image from "next/image";
import Link from "next/link";

const Footer = () => {
  return (
    <footer className="w-full bg-[#255855] pt-2 pb-3.5 lg:pb-6">
      <div className="flex justify-start px-6 [@media(min-width:1700px)]:px-[300px] [@media(min-width:2000px)]:px-[400px] lg:pl-28 lg:mt-6">
        <Image
          loader={imageLoader}
          alt="aria-logo-footer"
          src={"/logo-asset/aria-logo-text.png"}
          width={150}
          height={0}
          style={{ width: "150px", height: "auto" }}
          priority
        />
      </div>
      <div className="flex gap-4 flex-col px-6 [@media(min-width:1700px)]:px-[300px] [@media(min-width:2000px)]:px-[400px] lg:pl-32 lg:pr-20 lg:flex-row">
        <div className="order-last lg:order-1 lg:mt-4 lg:basis-[40%]">
          {renderAddress}
        </div>
        <div className="flex flex-col lg:order-last lg:flex-row lg:gap-x-2">
          {renderPages}
          {renderServices}
          {renderSubscribeForm}
        </div>
      </div>
      <div className="flex relative justify-center items-center mt-11 mb-3.5 px-6 [@media(min-width:1700px)]:px-[300px] [@media(min-width:2000px)]:px-[400px] lg:px-20 lg:mb-6">
        <hr className="border-[#517977] border-dashed w-full lg:border-solid" />
        <div className="h-2 w-3 rounded-full border-[1px] border-slate-500 lg:hidden" />
        <hr className="border-[#517977] border-dashed w-full lg:border-solid" />
      </div>
      <div className="flex justify-center">
        <span className="text-[#F6F6F6] text-[8px] lg:text-base">
          © 2023 ARIA. All rights reserved.
        </span>
      </div>
    </footer>
  );
};

const renderAddress = (
  <>
    <h2 className="font-semibold text-base text-[#F6F6F6] mt-4 mb-[1.12rem] lg:hidden">
      Contact US
    </h2>
    <div className="flex flex-col gap-3 mb-9 lg:gap-y-4">
      {_ADDRESS_CONTENT_.map(({ alt, image, desc }, i) => (
        <div
          key={i}
          className={`flex gap-4 text-white text-xs lg:text-sm ${
            i === 1 ? "items-center" : "items-start"
          }`}
        >
          <Image
            alt={alt}
            src={image}
            width={18}
            height={18}
            className={`${i === 0 ? "mt-1" : ""}`}
            priority
          />
          <span>{desc}</span>
        </div>
      ))}
    </div>
    <div className="text-[20px] grid grid-flow-col  px-10">
      <span className="text-white font-semibold text-[14px] sm:text-[20px]">
        Follow Us On
      </span>
      <div className="grid grid-flow-col sm:gap-5 gap-6 items-center justify-center">
        {_SOSMED_ITEMS_.map(({ href, alt, logo }, i) => (
          <Link href={href} key={i}>
            <Image alt={alt} src={logo} width={18} height={18} priority />
          </Link>
        ))}
      </div>
    </div>
  </>
);

const renderPages = (
  <div className="text-[#F6F6F6] flex flex-col order-2 text-xs gap-3 mb-5 lg:text-base lg:basis-[25%]">
    <h2 className="font-semibold text-base lg:text-xl">Page</h2>
    <Link href={"/about"}>
      <p>About</p>
    </Link>
    {/* <Link href={"/blog"}>
      <p>Blog</p>
    </Link> */}
    <Link href={"/contact-us"}>
      <p>Contact</p>
    </Link>
    <Link href={"https://www.kalibrr.com/id-ID/c/hiaria/jobs"}>
      <p>Careers</p>
    </Link>
    <p>Privacy Policy</p>
    <p>Terms & Condition</p>
  </div>
);

const renderServices = (
  <div className="text-[#F6F6F6] flex order-3 flex-col gap-3 text-xs lg:text-base lg:basis-[25%]">
    <h2 className="font-semibold text-base lg:text-xl">Our Services</h2>
    <Link href={"/aria-agriculture"}>
      <p>Aria Agriculture</p>
    </Link>
    <Link href={"/aria-tani"}>
      <p>Aria Tani</p>
    </Link>
    <div className="flex justify-start items-center gap-3 lg:gap-2">
      <p>Aria Mining</p>
      <div className="box-border px-2 py-1 bg-[#ea9000] text-[8px] text-center rounded grid content-center leading-3">
        <h3>coming soon</h3>
      </div>
    </div>
    <div className="flex justify-start items-center gap-3 lg:gap-2">
      <p>Aria Academy</p>
      <div className="box-border px-2 py-1 bg-[#7b1f9e] text-[8px] text-center rounded grid content-center leading-3">
        <h3>coming soon</h3>
      </div>
    </div>
  </div>
);

const renderSubscribeForm = (
  <div className="text-white order-1 flex lg:order-last lg:basis-[40%]">
    <div>
      <span className="font-semibold text-sm lg:text-xl">
        Subscribe to our newsletter to keep updated about our new product
      </span>
      <form className="w-full py-1 pb-5">
        <div className="flex items-center border-b border-white py-2">
          <input
            className="bg-transparent w-full border-transparent focus:border-transparent focus:ring-0 !outline-none"
            type="text"
            placeholder="Email Address"
          />
          <Image
            alt={"arrow-logo"}
            src={"/icons/footer/arrow.svg"}
            width={20}
            height={20}
            priority
          />
        </div>
      </form>
    </div>
  </div>
);

export default Footer;
