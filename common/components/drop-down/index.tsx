/** @format */

interface DropDowninputPropType {
  id?: string;
  label: string;
  placeholder?: string;
  name: string;
  required?: boolean;
  options: any;
  error?: any;
  value: string;
  onBlur: any;
  handleChange: any;
}

const DropDowninput = ({
  label,
  id,
  name,
  placeholder,
  required,
  options,
  error,
  onBlur,
  handleChange,
  value,
}: DropDowninputPropType): JSX.Element => {
  return (
    <div className="relative mb-[20px] sm:mb-[30px]">
      <label
        htmlFor={id || ""}
        className="block text-[12px] sm:text-[14px] text-[#808080] mb-2 sm:mb-0"
      >
        {label}
        {required && <span className="text-[#CC1B1B]">*</span>}
      </label>
      <div className="relative">
        <select
          id={id || ""}
          name={name || ""}
          value={value}
          onBlur={onBlur}
          onChange={handleChange}
          className={`bg-white sm:bg-[#F6F6F6] appearance-none text-gray-900 text-[14px] sm:text-[16px] rounded-[4px] w-full  block p-2.5 ${
            error
              ? "focus:outline-[#CC1B1B] border-[#CC1B1B]"
              : "focus:outline-[#1DAEEF] border-[#B2ADA7]"
          }`}
          placeholder={placeholder}
          required={required}
        >
          <option value="" disabled hidden>
            Choose product or service
          </option>
          {options.map((item: any, index: number) => (
            <option
              className="h-10 text-[#343434]"
              key={index}
              value={item.value}
            >
              {item.label}
            </option>
          ))}
        </select>
        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
          <svg
            className="fill-current h-4 w-4"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
          >
            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
          </svg>
        </div>
      </div>
      {error && (
        <p className="absolute italic text-[12px] sm:text-[14px] text-[#CC1B1B]">
          {error}
        </p>
      )}
    </div>
  );
};

export default DropDowninput;
