/** @format */

interface TextfieldPropType {
  id?: string;
  label: string;
  name: string;
  placeholder?: string;
  required?: boolean;
  error?: any;
  value: string;
  onBlur: any;
  handleChange: any;
}

const Textfield = ({
  label,
  id,
  name,
  value,
  handleChange,
  placeholder,
  required,
  onBlur,
  error,
}: TextfieldPropType): JSX.Element => {
  return (
    <div className="relative mb-[20px] sm:mb-[30px]">
      <label
        htmlFor={id || ""}
        className="block text-[12px] sm:text-[14px] text-[#808080] mb-2 sm:mb-0"
      >
        {label}
        {required && <span className="text-[#CC1B1B]">*</span>}
      </label>
      <input
        type="text"
        name={name || ""}
        id={id || ""}
        value={value}
        onBlur={onBlur}
        onChange={handleChange}
        className={`sm:bg-[#F6F6F6] text-gray-900 text-[14px] sm:text-[16px] rounded-[4px] w-full  block p-2.5 ${
          error
            ? "focus:outline-[#CC1B1B] border-[#CC1B1B]"
            : "focus:outline-[#1DAEEF] border-[#B2ADA7]"
        }`}
        placeholder={placeholder}
        required={required}
      />
      {error && (
        <p className="absolute italic text-[12px] sm:text-[14px] text-[#CC1B1B]">
          {error}
        </p>
      )}
    </div>
  );
};

export default Textfield;
