import { useState } from "react";
import Modal from "../modal";
import Image from "next/image";

const ContactUs = ({ color, buttonColor = "bg-[#255855]" }: any) => {
  const [open, setOpen] = useState(false);

  return (
    <div className="flex flex-col w-full py-12 px-6 bg-white sm:px-[100px] [@media(min-width:1700px)]:px-[300px]">
      <h3 className={`text-sm ${color} font-semibold mb-1 lg:text-4xl`}>
        Ready for a transformation?
      </h3>
      <p className="text-xs lg:text-4xl">
        Start your journey with us and let’s make an impact
      </p>
      <div className="mt-5">
        <button
          onClick={() => setOpen(true)}
          className={`${buttonColor} text-white px-2 py-1 sm:py-2 sm:px-4 rounded-full text-[12px] sm:text-[16px] border-[3px] border-transparent hover:border-[#57d4b6]`}
        >
          Contact us
        </button>
      </div>
      <ContactUsModal openState={[open, setOpen]} />
    </div>
  );
};

export default ContactUs;

import DropDowninput from "common/components/drop-down";
import Textfield from "common/components/text-field";
import * as yup from "yup";
import { useFormik } from "formik";
import axios from "axios";
import MessageModal from "../message-modal";

const isError = (formik: any, field: string) => {
  if (formik.touched[field] && formik.errors[field]) {
    return formik.errors[field];
  }
  return false;
};

export const _CONTACT_OPTIONS_ = [
  { label: "Area Mapping", value: "area-mapping" },
  { label: "Drone Spraying", value: "drone-spraying" },
  { label: "Data Platform", value: "data-plaform" },
  { label: "Digital Agronomist", value: "digital-agronomist" },
  { label: "Turbo Spreader", value: "turbo-spreader" },
  { label: "Worker Tracker", value: "worker-tracker" },
  { label: "Other", value: "other" },
];

const phoneIndonesia =
  /^(\+62 ((\d{3}([ -]\d{3,})([- ]\d{4,})?)|(\d+)))|(\(\d+\) \d+)|\d{3}( \d+)+|(\d+[ -]\d+)|\d+/i;

const validationSchema = yup.object({
  firstName: yup.string().required("First name is required"),
  lastName: yup.string().required("Last name is required"),
  email: yup.string().email().required("Enter a valid email"),
  phone: yup
    .string()
    .matches(phoneIndonesia, "Phone number is not valid")
    .matches(/^\d+$/, "Phone number is not valid")
    .required("Please input your phone number"),
  service: yup.string().required("Please choose a product or service"),
});

const ContactUsModal = ({ openState }: any) => {
  const [openMessage, setOpenMessage] = useState(false);
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
      service: "",
      note: "",
    } as any,
    validationSchema: validationSchema,
    onSubmit: async (values, actions) => {
      await contactUs(values);
      setOpen(false);
      formik.resetForm({
        values: {
          firstName: "",
          lastName: "",
          email: "",
          phone: "",
          service: "",
          note: "",
        },
      });
      setOpenMessage(true);
      setTimeout(() => setOpenMessage(false), 3000);
    },
  });
  const [open, setOpen] = openState;
  return (
    <>
      <MessageModal
        onClickOutside={() => setOpenMessage(false)}
        open={openMessage}
      >
        <div className="flex items-center p-3 px-5">
          <div className="mr-5">
            <Image
              alt={"success icon"}
              src={"/success_icon.svg"}
              width={60}
              height={60}
              priority
            />
          </div>
          <div>
            <p>Thank You</p>
            <p className="text-[#878787] text-[12px]">
              We’ll respond to you shortly!
            </p>
          </div>
        </div>
      </MessageModal>
      <Modal
        onClickOutside={() => setOpen(false)}
        open={open}
        title={`Contact us`}
      >
        <div className="p-5 sm:min-w-[680px]">
          <form noValidate onSubmit={formik.handleSubmit}>
            <Textfield
              label={"First name"}
              id={"firstName"}
              placeholder={"e.g. John"}
              name={"firstName"}
              value={formik.values.firstName}
              handleChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={isError(formik, "firstName")}
              required
            />
            <Textfield
              label={"Last Name"}
              id={"lastName"}
              placeholder={"e.g. Doe"}
              name={"lastName"}
              value={formik.values.lastName}
              handleChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={isError(formik, "lastName")}
              required
            />
            <Textfield
              label={"Email Address"}
              id={"email"}
              placeholder={"e.g. hiaria@email.com"}
              name={"email"}
              value={formik.values.email}
              handleChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={isError(formik, "email")}
              required
            />
            <Textfield
              label={"Phone Number"}
              id={"phone"}
              placeholder={"e.g. 083784758695"}
              name={"phone"}
              value={formik.values.phone}
              handleChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={isError(formik, "phone")}
              required
            />
            <DropDowninput
              label={"Product Or Service"}
              id={"service"}
              options={_CONTACT_OPTIONS_}
              name={"service"}
              value={formik.values.service}
              handleChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={isError(formik, "service")}
              required
            />
            <Textfield
              label={"Note"}
              id={"note"}
              placeholder={"e.g. i want to try out area mapping"}
              name={"note"}
              value={formik.values.note}
              handleChange={formik.handleChange}
              onBlur={formik.handleBlur}
              error={isError(formik, "note")}
            />
            <div className="mt-6">
              <button
                type="submit"
                className="bg-[#255855] hover:bg-[#386C6A] Text-white text-white py-2 w-full rounded-[25px] text-[16px] border-[3px] border-transparent hover:border-[#57d4b6] "
              >
                Send
              </button>
            </div>
          </form>
        </div>
      </Modal>
    </>
  );
};

// handler
const contactUs = (form: any) => {
  return axios.post(process.env.USER_SERVICE_API_URL || "", form, {
    headers: form.options?.headers,
  });
};
