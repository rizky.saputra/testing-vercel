import { _SUCCSESS_STORIES_ } from "common/constant";
import Chip from "../chip";

const ClientSuccsessStories = ({ color }: any) => {
  return (
    <div className="flex flex-col px-6 h-full bg-white lg:px-28 pt-6">
      <h3 className={`text-sm ${color} lg:text-4xl`}>Client Success Stories</h3>
      <p className="text-xs font-bold lg:text-4xl lg:font-normal lg:text-[#404040]">
        Use & Winning Cases
      </p>
      <div className="grid grid-cols-1 gap-8 mt-5 lg:grid-cols-3">
        {_SUCCSESS_STORIES_.map((item, index) => {
          return (
            <div key={index} className="">
              <div className="relative w-full h-[204px] bg-gray-200 rounded-[22px] mb-4">
                <Chip title={item.category} />
              </div>
              <p className="text-xs font-bold text-[#808080] mb-2">
                {item.date} . {item.name}
              </p>
              <h3 className="text-sm text-[#404040] mb-2 hover:text-[#386C6A] cursor-pointer lg:text-xl lg:font-semibold">
                {item.title}
              </h3>
              <h4 className="text-xs font-bold text-[#808080] lg:text-base lg:font-normal">
                {item.excerpt}
              </h4>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default ClientSuccsessStories;
