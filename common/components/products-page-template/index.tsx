/** @format */

import ClientSuccsessStories from "common/components/client-succsess-story";
import ContactUs from "common/components/contact-us";
import { imageLoader } from "common/utils";
import useWindowSize from "@/hooks/useWindowSize";
import Head from "next/head";
import Image from "next/image";

const ProductsPageTemplate = ({ data: { meta }, data }: any) => {
  const bannerType = data.outputExample.bannerExample.type;
  const dataTitle = data.outputExample.title;
  const mobileImage = data.outputExample.mobileImage;

  const renderFeature = (
    <section className="flex flex-col px-6 mb-8 lg:mb-20 sm:px-[100px] [@media(min-width:1700px)]:px-[300px] text-[#F6F6F6]">
      <div className="mt-4 mb-3 text-sm lg:text-4xl lg:mb-8 lg:mt-20">
        <h2 className="text-[#255855] font-semibold lg:mb-1">
          {data.features.title}
        </h2>
        <p className="text-[#404040] font-normal">
          {data.features.description}
        </p>
      </div>
      <div className="grid grid-cols-2 gap-2.5 lg:grid-cols-4 lg:gap-5">
        {data.features.services.map((item: any, index: any) => {
          return (
            <div
              key={index}
              className={`relative group drop-shadow-aria bg-white rounded-[24px] sm:min-h-[17rem] min-h-[11rem] ${
                index > 0 ? "col-span-1" : "row-span-2"
              }`}
            >
              <div
                className={`group-hover:hidden flex flex-col h-full w-full ${
                  index > 0 ? "pt-[3.2rem] sm:pt-16" : "justify-center"
                } items-center`}
              >
                <div className="w-[60px] h-[60px] sm:w-[100px] sm:h-[100px]">
                  <Image
                    loader={imageLoader}
                    alt={item.title}
                    src={item.icon}
                    width={100}
                    height={100}
                  />
                </div>

                <div className="mt-2 px-6">
                  <p className="font-semibold text-xs text-[#404040] text-center lg:text-xl">
                    {item.title}
                  </p>
                </div>
              </div>
              <div className="hidden group-hover:flex h-full relative">
                <div className="bg-[#255855] h-full w-full absolute top-0 z-[-1] card-masking rounded-[24px]" />
                <div className="bg-[#255855] brightness-[1.15] h-full w-full absolute top-0 z-[-20] rounded-[24px]" />
                <div className="sm:p-5 p-3 mih-h-[70px]">
                  <div className="w-[40px] h-[40px] sm:w-[70px] sm:h-[70px]">
                    <Image
                      loader={imageLoader}
                      alt={item.title}
                      src={item.iconHover}
                      width={70}
                      height={70}
                    />
                  </div>

                  <p className="font-semibold text-xs text-white mt-2 mb-1 lg:text-sm">
                    {item.titleHover}
                  </p>
                  <p className="text-[10px] text-white lg:text-sm">
                    {item.description}
                  </p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </section>
  );

  const renderOutput = (
    <section
      className={`flex flex-col px-6 sm:px-[100px] [@media(min-width:1700px)]:px-[300px] ${
        data.outputExample.bgColor
      } lg:px-28 lg:mb-10 ${
        dataTitle === "Nutrition Map Example" && "lg:mb-0 lg:pb-8"
      }`}
    >
      {data.outputExample.title && (
        <h2
          className={`text-sm font-semibold text-[#255855] my-4 lg:text-4xl lg:mb-12 ${
            dataTitle === "Nutrition Map Example" && "lg:mt-12"
          }`}
        >
          {data.outputExample.title}
        </h2>
      )}
      {data.outputExample.outputItemExamples.length > 0 && (
        <div className="grid grid-cols-1 gap-3 sm:gap-5 mb-8 lg:grid-cols-2">
          {data.outputExample.outputItemExamples.map(
            (item: any, index: any) => (
              <div
                key={index}
                className="h-16 relative flex items-center lg:h-28"
              >
                <div className="bg-[#255855] h-full w-full absolute top-0 z-[-1] card-masking rounded-2xl" />
                <div className="bg-[#386C6A] h-full w-full absolute top-0 z-[-20] rounded-2xl" />
                <div className="px-6 w-full flex flex-row items-center">
                  <Image
                    loader={imageLoader}
                    alt={item.title}
                    src={item.image}
                    width="0"
                    height="0"
                    priority
                    className="w-10 lg:w-20"
                  />
                  <div className="ml-3 lg:ml-5">
                    <p className="text-white text-xs font-semibold lg:text-2xl">
                      {item.title}
                    </p>
                    <p className="text-white text-[8.9px] sm:text-[16px] leading-4 font-semibold">
                      {item.description}
                    </p>
                  </div>
                </div>
              </div>
            )
          )}
        </div>
      )}
      {data.outputExample.outputImageExamples.length > 0 && (
        <div
          className={`${
            dataTitle === "Drone Spraying on Data Platform" && "pb-7 lg:pb-1"
          }`}
        >
          {mobileImage && (
            <div className="flex w-full sm:hidden md:hidden lg:hidden xl:hidden 2xl:hidden">
              <Image
                loader={imageLoader}
                alt={mobileImage.alt}
                src={mobileImage.image}
                width="0"
                height="0"
                className="w-full h-full"
              />
            </div>
          )}
          {data.outputExample.outputImageExamples.map(
            (item: any, index: any) => (
              <div
                key={index}
                className={`flex w-full justify-center ${
                  mobileImage &&
                  "hidden sm:flex md:flex lg:flex xl:flex 2xl:flex"
                }`}
              >
                <Image
                  loader={imageLoader}
                  alt={item.alt}
                  src={item.image}
                  width={940}
                  height={311}
                  className="w-[85%]"
                  priority
                />
              </div>
            )
          )}
        </div>
      )}
    </section>
  );

  const renderReport = (
    <div className="">
      <div className="bg-white w-full h-full absolute z-[-1]" />
      <Image
        loader={imageLoader}
        alt={data.servicePreview.title}
        src={data.servicePreview.backgroundImage}
        width={940}
        height={311}
        className="absolute w-[100%] 2xl:max-h-[60%] z-[-1]"
        priority
      />
      <h2 className="text-xs text-normal text-white my-3 text-center lg:text-4xl lg:mt-12 lg:mb-10">
        {data.servicePreview.title}
      </h2>
      <div className="w-full h-[9.5rem] flex justify-center lg:h-[32.5rem]">
        <Image
          loader={imageLoader}
          alt={data.servicePreview.title}
          src={data.servicePreview.image}
          width={940}
          height={311}
          className="absolute w-[60%] 2xl:max-w-[45%] 2xl:max-h-[90%] z-[-1]"
          priority
        />
      </div>
    </div>
  );

  const renderClientHistory = (
    <div
      className={`relative flex flex-col bg-white ${
        bannerType === "withTopTitle" && "items-center"
      } ${
        data.outputExample.bannerExample.title === "Turbo Spreader Device"
          ? ""
          : "sm:px-[100px] [@media(min-width:1700px)]:px-[300px]"
      }`}
    >
      {bannerType === "withTopTitle" && (
        <div className="flex items-center w-full pt-3 mb-2.5 lg:pt-12 lg:mb-10">
          <p className="text-sm text-[#255855] font-semibold lg:text-4xl">
            {data.outputExample.bannerExample.title}
          </p>
        </div>
      )}
      <Image
        loader={imageLoader}
        alt={data.outputExample.bannerExample.alt}
        src={data.outputExample.bannerExample.image}
        width="0"
        height="0"
        className={`${
          bannerType === "withTopTitle" ? "w-[100%] lg:mb-3.5" : "w-[100%]"
        } h-auto`}
        priority
      />
      {bannerType === "withMiddleTitle" && (
        <div className="absolute ml-7 top-1/3 lg:ml-28">
          <p className="text-xs text-[#F6F6F6] lg:text-4xl">
            {data.outputExample.bannerExample.title}
          </p>
        </div>
      )}
    </div>
  );

  return (
    <>
      <Head>
        <title>{meta.title}</title>
        <meta name="description" content={meta.description} />
        <meta name="keyword" content={meta.keyword} />
        <meta property="og:title" content={meta.title} />
        <meta property="og:description" content={meta.description} />
      </Head>
      <ServiceHero
        title={data.title}
        description={data.description}
        banner={data.banner}
        mobileBanner={data.mobileBanner}
      />
      {renderFeature}
      {renderOutput}
      {renderReport}
      {data.outputExample.bannerExample.title && renderClientHistory}
      {/* <ClientSuccsessStories color="text-[#255855]" /> */}
      <ContactUs color="text-[#255855]" />
    </>
  );
};

export default ProductsPageTemplate;

const ServiceHero = ({
  title,
  description,
  banner,
  mobileBanner = "",
}: any) => {
  const { width } = useWindowSize();
  const imageBanner = width < 640 && mobileBanner;

  return (
    <div className="relative h-52 lg:h-[37rem]">
      <Image
        loader={imageLoader}
        alt={title.row1 + title.row2}
        src={imageBanner ? mobileBanner : banner}
        fill
        priority
      />
      <div className="absolute text-center flex flex-col justify-center h-full px-6 sm:px-[100px] [@media(min-width:1700px)]:px-[300px] lg:text-left lg:top-2">
        <h1 className="text-white font-semibold text-3xl lg:text-6xl">
          {title.row1}
          <span className="text-[#07A9A0] inline-block ml-1 text-3xl lg:block lg:ml-auto lg:text-6xl">
            {title.row2}
          </span>
        </h1>
        <div className="text-left pt-2 lg:pt-7 lg:w-[37.5rem]">
          <p className="text-white font-normal text-xs lg:text-xl">
            <span>{description.row1}</span>{" "}
            <span>{description.row2 ?? ""}</span>{" "}
            <span>{description.row3 ?? ""}</span>
          </p>
        </div>
      </div>
    </div>
  );
};
