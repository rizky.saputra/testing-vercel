import { useEffect, useRef, useState } from "react";
import Modal from "../modal";

const MessageModal = ({ onClickOutside, open, children, title }: any) => {
  const ref: any = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (ref.current && !ref.current.contains(event.target)) {
        onClickOutside && onClickOutside();
      }
    };
    document.addEventListener("click", handleClickOutside, true);
    return () => {
      document.removeEventListener("click", handleClickOutside, true);
    };
  }, [onClickOutside]);

  return (
    <>
      {open && (
        <div>
          <div
            className={`justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none`}
          >
            <div className="relative w-auto my-6 mx-auto" ref={ref}>
              <div className="border-0 border-l-[6px] border-[#1B974E] rounded-[16px] shadow-lg relative flex flex-col w-full outline-none focus:outline-none bg-[#F6F6F6]">
                {children}
              </div>
            </div>
          </div>
          <div className={`fixed inset-0 z-40`}></div>
        </div>
      )}
    </>
  );
};

export default MessageModal;
