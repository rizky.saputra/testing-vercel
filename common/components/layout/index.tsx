/** @format */

import { _NAVBAR_ } from "common/constant";
import type { AppProps } from "next/app";
import Footer from "../footer";
import Navbar from "../navbar";

const Layout = ({ Component, pageProps, ...appProps }: AppProps) => {
  return (
    <div className="min-h-screen flex flex-col">
      <Navbar />
      <Component {...pageProps} {...appProps} />
      <Footer />
    </div>
  );
};

export default Layout;
