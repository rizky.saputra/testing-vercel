import Chip from "common/components/chip";
import { _FEATURED_POST_ } from "modules/blog/constant";

const FeaturedPost = () => {
  return (
    <div className="absolute 2xl:top-[600px] top-[620px] w-full h-[300px]">
      <div className="flex justify-between px-[100px] [@media(min-width:1800px)]:px-[200px] [@media(min-width:2000px)]:px-[300px]">
        {_FEATURED_POST_.map((post, index) => {
          return (
            <div
              key={index}
              className="w-[266px] h-[266px] 2xl:w-[350px] 2xl:h-[350px] rounded-[20px] drop-shadow-aria bg-white"
            >
              <div className="flex flex-col h-full">
                <div className="relative basis-2/3 bg-gray-200 rounded-t-[20px]">
                  <Chip title={post.category} />
                </div>
                <div className="basis-1/3 rounded-b-[20px] p-4">
                  <h2 className="text-[22px] text-[#404040] font-semibold hover:text-[#386C6A] cursor-pointer">
                    {post.title}
                  </h2>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default FeaturedPost;
