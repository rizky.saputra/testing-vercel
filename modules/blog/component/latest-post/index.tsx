import Chip from "common/components/chip";
import { _LATEST_POST_ } from "modules/blog/constant";

const LatestPost = () => {
  return (
    <div className="grid grid-cols-2 2xl:grid-cols-3 gap-16">
      {_LATEST_POST_.map((item, index) => {
        return (
          <div key={index} className="h-[388px]">
            <div className="relative w-full h-[204px] bg-gray-200 rounded-[22px] mb-2">
              <Chip title={item.category} />
            </div>
            <p className="text-[12px] text-[#808080] mb-2">
              {item.date} . {item.name}
            </p>
            <h3 className="text-[22px] font-semibold text-[#404040] mb-2 hover:text-[#386C6A] cursor-pointer">
              {item.title}
            </h3>
            <h4 className="text-[16px] text-[#808080]">{item.excerpt}</h4>
          </div>
        );
      })}
    </div>
  );
};

export default LatestPost;
