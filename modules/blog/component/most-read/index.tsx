import Chip from "common/components/chip";
import { _MOST_READ_ } from "modules/blog/constant";

const MostRead = () => {
  return (
    <div className="bg-white rounded-[20px] p-5 drop-shadow-aria">
      <h3 className="text-[#808080] text-[22px] mb-4">Most Read</h3>
      {_MOST_READ_.map((item, index) => {
        return (
          <div key={index} className="flex">
            <div className="basis-1/6">
              <p className="text-[22px] font-semibold text-[#404040]">
                #{index + 1}
              </p>
            </div>
            <div className="basis-5/6">
              <div className="relative rounded-[12px] w-[270px] h-[142px] bg-gray-200 mb-3">
                <Chip title={item.category} />
              </div>
              <p className="text-[12px] text-[#808080] mb-1">
                {item.date} . {item.name}
              </p>
              <h3 className="text-[22px] font-semibold text-[#404040] mb-5 hover:text-[#386C6A] cursor-pointer">
                {item.title}
              </h3>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default MostRead;
