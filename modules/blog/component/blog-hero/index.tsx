const BlogHero = () => {
  return (
    <div className="bg-gray-700 h-[646px]">
      <div className="flex px-[100px] [@media(min-width:1800px)]:px-[200px] [@media(min-width:2000px)]:px-[300px] h-full items-center">
        <h1 className="text-[64px] text-[#FFFFFF] font-bold mb-[90px] ">
          All New Stories, Tips, & Insight
          <span className="text-[#1B974E] font-barlow block">
            on Aria Indonesia
          </span>
        </h1>
      </div>
    </div>
  );
};

export default BlogHero;
