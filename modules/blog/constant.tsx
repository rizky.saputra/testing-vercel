export const _FEATURED_POST_ = [
  {
    title: "Agriculture & Data Insights",
    category: "Agriculture",
    image: "image",
  },
  {
    title: "Sustainability Development Goals",
    category: "Agriculture",
    image: "image",
  },
  {
    title: "Client & Farmers Testimonials",
    category: "Agriculture",
    image: "image",
  },
  {
    title: "Media Coverage",
    category: "Promotions",
    image: "image",
  },
  // {
  //   title: "Media Coverage",
  //   category: "Promotions",
  //   image: "image",
  // },
  // {
  //   title: "Media Coverage",
  //   category: "Promotions",
  //   image: "image",
  // },
];

export const _LATEST_POST_ = [
  {
    title: "How Russian - Ukraine War Effected to Food Security?",
    excerpt:
      "Russian - Ukraine war has effected to food chain around the world. A lot of country feel the food crisis. How it can be solve?",
    date: "Mon, 12 December 2022",
    name: "By Jane Doe",
    category: "Business",
  },
  {
    title: "10 Tips for Pests and Diseases Management in Agriculture",
    excerpt:
      "A lot of mistakes that we’ve made while farming which leads to crop failure. How we can prevent any crop failure?",
    date: "Mon, 12 December 2022",
    name: "By Jane Doe",
    category: "Lifestyle",
  },
  {
    title: "Challenges of Modern Agriculture, is It Complex?",
    excerpt:
      "Modern agriculture has many complex challenges for farmers that hard for them to survive. ",
    date: "Mon, 12 December 2022",
    name: "By Jane Doe",
    category: "Business",
  },
  {
    title: "What Is Agricultural Business?",
    excerpt:
      "Agriculture business has raising in Indonesia and become government's attention to increasing food security. What it is all about?",
    date: "Mon, 12 December 2022",
    name: "By Jane Doe",
    category: "Agriculture",
  },
  {
    title: "Get to Know About Aria Digital Agronomist and its Benefit",
    excerpt:
      "Aria digital agronomist able to help farmers and owners to prevent any diseases, able to seed properly, and keep plant healthy. ",
    date: "Mon, 12 December 2022",
    name: "By Jane Doe",
    category: "Health",
  },
  {
    title: "Aria Attend Kaesang - Erina Wedding",
    excerpt:
      "Aria has a chance to open booth in Kaesang - Erina Wedding, let’s read out story. ",
    date: "Mon, 12 December 2022",
    name: "By Jane Doe",
    category: "Business",
  },
];

export const _MOST_READ_ = [
  {
    title: "Automation in Indonesia’s Farming Industry",
    date: "Fri, 12 August 2022",
    name: "By Jane Doe",
    category: "Business",
  },
  {
    title: "Aria Joins B20",
    date: "Tue, 14 November 2022",
    name: "By Jane Doe",
    category: "Business",
  },
  {
    title: "Biggest Agriculture Event Held in Jakarta",
    date: "Thu, 28 July 2022",
    name: "By Jane Doe",
    category: "Business",
  },
];
