/** @format */

import Head from "next/head";
import BlogHero from "./component/blog-hero";
import FeaturedPost from "./component/featured-post";
import LatestPost from "./component/latest-post";
import MostRead from "./component/most-read";

const BlogModule = () => {
  return (
    <>
      <Head>
        <title>The Latest Aria Indonesia Stories</title>
        <meta
          name='description'
          content='All New Stories, Tips, & Insight on Aria Indonesia'
        />
        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <div className='bg-[#f6f6f6]'>
        <BlogHero />
        <div className='mb-[200px] 2xl:mb-[270px]'>
          <FeaturedPost />
        </div>
        <div className='px-[100px] [@media(min-width:1800px)]:px-[200px] [@media(min-width:2000px)]:px-[300px]'>
          <h2 className='text-[#1B974E] text-[36px] font-semibold mb-5'>
            The Latest
            <span className='text-[#404040] block'>Aria Indonesia Stories</span>
          </h2>
          <div className='flex'>
            <div className='basis-2/3 2xl:basis-3/4'>
              <LatestPost />
            </div>
            <div className='basis-1/3 2xl:basis-1/4 pl-6'>
              <MostRead />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default BlogModule;
