/** @format */

export const _MILESTONES_ = [
  {
    number: 400,
    unit: "k+",
    description: "Hectares of mapping",
  },
  {
    number: 65,
    unit: "k+",
    description: "Hectares drone spraying",
  },
  {
    number: 457,
    description: "Clients satisfied",
  },
  {
    number: 125,
    description: "Numbers of pilot",
  },
];

export const _OUR_PRODUCT_ = [
  {
    productlogo: "/logo-asset/aria-agriculture-logo.png",
    titleproduct: "Aria Agriculture",
    descproduct:
      "All agriculture-related technologies aim to enhance plantation and forestry companies for a better yield quality and quantity.",
  },
  {
    productlogo: "/logo-asset/aria-logistic-logo.png",
    titleproduct: "Aria Logistic",
    descproduct:
      "Other drone-related services empower business with data analytics, object detection, machine learning, and drone automation.",
  },
  {
    productlogo: "/logo-asset/aria-asset-logo.png",
    titleproduct: "Aria Assets",
    descproduct:
      "Computerized asset management and tracking system to support the operational activity and locate the asset's whereabouts.",
  },
];

export const _MISI_ = [
  {
    image: "/logo-asset/research-logo.svg",
    title: "Research",
    desc: "ARIA’s Research & Development department is founded to cater to custom business needs of multiple verticals, creating and proofing concepts dictated by markets.",
  },
  {
    image: "/logo-asset/efficiency-logo.svg",
    title: "Efficiency",
    desc: "Drone labor will multiple task efficiency and completion rates. Each drone can perform tasks up to 2.5x faster for mapping, fertilizing and inventory audits. Deploying a swarm of drones will turn tasks that once took multiple days, into hours.",
  },
  {
    image: "/logo-asset/productivity-logo.svg ",
    title: "Productivity",
    desc: "The efficiency, quality and resource saving nature of drone labor will quickly reveal themselves into a quick ROI process for its users. Taking months instead of years to return client investments into operating profits.",
  },
];

export const _OUR_PARTNER_ = [
  {
    alt: "fap-logo",
    picture: "/logo-asset/fap-logo.webp",
  },
  {
    alt: "bayer-logo",
    picture: "/logo-asset/bayer-logo.webp",
  },
  {
    alt: "syngenta-logo",
    picture: "/logo-asset/syngenta-logo.webp",
  },
  {
    alt: "sampoerna-logo",
    picture: "/logo-asset/sampoerna-logo.webp",
  },
  {
    alt: "sag-logo",
    picture: "/logo-asset/sag-logo.webp",
  },
  {
    alt: "sinarmas-logo",
    picture: "/logo-asset/sinarmas-logo.webp",
  },
  {
    alt: "triputra-logo",
    picture: "/logo-asset/triputra-logo.webp",
  },
  {
    alt: "linfox-logo",
    picture: "/logo-asset/linfox-logo.webp",
  },
];

export const _OUR_SOLUTION_ = [
  {
    title: "Aria Agriculture",
    logo: {
      alt: "aria-agriculture",
      icon: "/background-image/home/group-agriculture.svg",
    },
    desc: {
      row1: "All agriculture-related technologies aim to enhance plantation and forestry companies",
      row2: "for a better yield quality and quantity.",
    },
    url: "/aria-agriculture",
    backgroundColor: "bg-[#255855]",
    items: [
      {
        product: "Area Mapping",
        detail:
          "Capture geographic information and visuals, tree counting, gain plant nutrition insights, and locate sick crops.",
        icon: "/logo-asset/aria-agriculture/area-mapping-agri.svg",
        url: "/aria-agriculture/area-mapping",
      },
      {
        product: "Drone Spraying",
        detail:
          "Autonomous fertilizer and pesticide spraying to swarm economic efficiency.",
        icon: "/logo-asset/aria-agriculture/drone-spraying-agri.svg",
        url: "/aria-agriculture/drone-spraying",
      },
      {
        product: "Data Platform",
        detail:
          "All-in-one platform to manage, gain insight, and view reports from Aria’s services.",
        icon: "/logo-asset/aria-agriculture/data-platform-agri.svg",
        url: "/aria-agriculture/data-platform",
      },
      {
        product: "Digital Agronomist",
        detail:
          "Measure and predict macronutrient levels (N, P, K), receive fertilizer recommendations, and achieve precision agriculture. ",
        icon: "/logo-asset/aria-agriculture/digital-agronomist-agri.svg",
        url: "/aria-agriculture/digital-agronomist",
      },
      {
        product: "Turbo Spreader",
        detail:
          "Mechanical fertilizing with chemical usage detection, precision spraying, and data analysis on Aria Data Platform.",
        icon: "/logo-asset/aria-agriculture/turbo-spreader-agri.svg",
        url: "/aria-agriculture/turbo-spreader",
      },
      {
        product: "Worker Tracker",
        detail:
          "Track movements and area coverage for efficient work and harvest monitoring.",
        icon: "/logo-asset/aria-agriculture/worker-tracker-agri.svg",
        url: "/aria-agriculture/worker-tracker",
      },
    ],
  },
  {
    title: "Aria Mining",
    logo: {
      alt: "aria-mining",
      icon: "/background-image/home/group-mining.svg",
    },
    desc: {
      row1: "Implement Aria’s mapping services in the mining sector to reduce the risk of landslides",
      row2: "and accidents and ensure businesses can run safely and optimally.",
    },
    url: "/",
    backgroundColor: "bg-[#EA9000]",
    items: [],
  },
  {
    title: "Aria Tani",
    logo: {
      alt: "aria-tani",
      icon: "/background-image/home/group-ariatani.svg",
    },
    desc: {
      row1: "Aria Tani application offers our clients and farmers to consult with our agronomist,",
      row2: "ordering products, services, and building communities.",
    },
    url: "/aria-tani",
    backgroundColor: "bg-[#1B974E]",
    items: [
      {
        product: "Area Mapping",
        detail:
          "Capture geographic information and visuals, tree counting, gain plant nutrition insights, and locate sick crops.",
        icon: "/logo-asset/aria-tani/area-mapping-tani.svg",
        url: "/aria-tani/area-mapping",
      },
      {
        product: "Drone Spraying",
        detail:
          "Autonomous fertilizer and pesticide spraying to swarm economic efficiency.",
        icon: "/logo-asset/aria-tani/drone-spraying-tani.svg",
        url: "/aria-tani/drone-spraying",
      },
      {
        product: "Digital Agronomist",
        detail:
          "Measure and predict macronutrient levels (N, P, K), receive fertilizer recommendations, and achieve precision agriculture.",
        icon: "/logo-asset/aria-tani/digital-agronomist-tani.svg",
        url: "/aria-tani/digital-agronomist",
      },
    ],
  },
  {
    title: "Aria Academy",
    logo: {
      alt: "aria-academy",
      icon: "/background-image/home/group-academy.svg",
    },
    desc: {
      row1: "Aria Academy is an educational program to train aspiring field managers and young",
      row2: "farmers and to support economic sustainability in the agriculture sector.",
    },
    url: "/",
    backgroundColor: "bg-[#7B1F9E]",
    items: [],
  },
];
