/** @format */

import Image from "next/image";
import { _MISI_ } from "modules/home/constant";
import { imageLoader } from "common/utils";

const Misi = () => {
  return (
    <div className="grid px-11 pb-8 grid-cols-1 lg:grid-cols-3 sm:px-[100px] [@media(min-width:1700px)]:px-[300px] lg:pb-20 lg:pt-8 lg:gap-5">
      {_MISI_.map((misi, index) => (
        <div className="p-4 lg:p-0" key={index}>
          <div className="box-border rounded-xl shadow-[0_3px_22px_rgba(149,175,199,0.2)] bg-white h-60 lg:bg-[#F6F6F6] lg:pt-2 lg:h-full lg:pb-4">
            <div className="flex place-items-center">
              <Image
                loader={imageLoader}
                alt={misi.title}
                src={misi.image}
                width="0"
                height="0"
                priority
                className="w-14 h-14 lg:h-[4.5rem] lg:w-20"
              />
            </div>
            <h1 className="px-4 leading-8 text-[#404040] font-semibold lg:text-[22px] lg:leading-[3rem] lg:px-6">
              {misi.title}
              <p className="text-xs text-[#808080] font-normal lg:text-base">
                {misi.desc}
              </p>
            </h1>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Misi;
