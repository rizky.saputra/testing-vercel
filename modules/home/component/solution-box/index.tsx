/** @format */

import { imageLoader } from "common/utils";
import Link from "next/link";
import Image from "next/image";
import { CarouselProvider, Slider, Slide } from "pure-react-carousel";

const SolutionBox = ({ logo, desc, items = [], url, backgroundColor }: any) => {
  const renderCarousel = () => {
    const newItems = [
      ...items,
      {
        product: "Discover More",
        url: items.length > 3 ? "/aria-agriculture" : "/aria-tani",
      },
    ];
    return (
      items.length > 0 && (
        <CarouselProvider
          className="relative"
          naturalSlideWidth={100}
          naturalSlideHeight={100}
          totalSlides={newItems.length}
          visibleSlides={3}
        >
          <Slider trayTag={"div"} className="pl-7">
            {newItems.map((item: any, index: number) => (
              <Slide key={index} index={index}>
                <SliderItems
                  item={item}
                  items={items}
                  index={index}
                  colSpan={1}
                  url={url}
                  color={backgroundColor}
                  colCount={1}
                />
              </Slide>
            ))}
          </Slider>
        </CarouselProvider>
      )
    );
  };

  return (
    <div className="relative flex h-full items-center mt-16">
      <div className="absolute top-[-1.5rem] left-4 lg:top-[-3rem] lg:left-0">
        <div className="w-32 h-11 lg:w-64 lg:h-20">
          <Image
            loader={imageLoader}
            alt={logo.alt}
            src={logo.icon}
            fill
            priority
          />
        </div>
      </div>
      <div className="w-full">
        <div
          className={`${backgroundColor} pt-8 pb-6 lg:rounded-2xl lg:px-10 lg:mb-16`}
        >
          <div
            className={`relative flex flex-col gap-4 px-7 lg:flex-row  lg:gap-20 lg:items-center ${
              items.length > 1 && "hidden lg:flex md:flex"
            }`}
          >
            <div className="text-xs text-[#F6F6F6] leading-[18px] lg:text-base lg:py-4">
              <p>{`${desc.row1} ${desc.row2}`}</p>
            </div>
            {items.length < 1 && (
              <div className="justify-end flex">
                <div className="rounded-[9px] bg-[rgba(246,246,246,0.7)] flex items-center justify-center py-2.5 px-4 lg:w-max lg:rounded-3xl">
                  <p className="opacity-100 text-xs hover:scale-x-105 hover:scale-y-110 transition-all font-semibold cursor-pointer lg:text-[26px] lg:py-4">
                    Serving Soon
                  </p>
                </div>
              </div>
            )}
          </div>
          {items.length > 0 && (
            <>
              <div className="block md:hidden lg:hidden">
                {renderCarousel()}
              </div>
              <div className="grid-cols-4 row-2 mt-8 gap-7 hidden lg:grid md:grid">
                {items.map((item: any, index: number) => {
                  const colSize = (items: any) => {
                    const len = items.length;
                    if (len < 4) {
                      const rest = 4 - len;
                      return rest;
                    }
                    if (len >= 4) {
                      const rest = 8 - len;
                      return rest;
                    }
                    return 0;
                  };

                  const colCount = colSize(items);

                  let colSpan = `col-span-1`;

                  switch (colCount) {
                    case 2:
                      colSpan = `col-span-2`;
                      break;
                    case 3:
                      colSpan = `col-span-3`;
                      break;
                    case 4:
                      colSpan = `col-span-4`;
                      break;
                    default:
                      colSpan;
                  }

                  return (
                    <SolutionBoxItem
                      item={item}
                      key={index}
                      items={items}
                      index={index}
                      colSpan={colSpan}
                      url={url}
                      color={backgroundColor}
                      colCount={colCount}
                    />
                  );
                })}
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default SolutionBox;

const SolutionBoxItem = ({
  item,
  items,
  index,
  colSpan,
  url,
  color,
  colCount,
}: any) => {
  const bgColor = `hover:${color}`;
  return (
    <>
      <div
        className={`group ${bgColor} relative w-full h-[250px] rounded-[24px] z-10 bg-white flex flex-col justify-center items-center`}
      >
        <div className="absolute group-hover:hidden flex flex-col justify-center items-center h-full pt-7 px-auto w-full">
          <Image
            loader={imageLoader}
            alt={"title"}
            src={item.icon}
            width={100}
            height={100}
          />
          <div className="min-h-[70px] mt-1">
            <p className="font-semibold text-[22px] text-[#404040] text-center">
              {item.product}
            </p>
          </div>
        </div>
        <div className="hidden group-hover:flex w-full h-full relative">
          <div
            className={`${color} h-full w-full absolute top-0 z-[-1] card-masking rounded-[24px]`}
          />
          <div
            className={`${color} brightness-[1.15] h-full w-full absolute top-0 z-[-20] rounded-[24px]`}
          />
          <div className="flex flex-col justify-between p-5 2xl:p-8">
            <div>
              <p className="text-[16px] text-white">{item.detail}</p>
            </div>
            <div className="flex justify-end">
              <Link
                href={item.url}
                className="bg-white/20 px-5 py-2 rounded-[24px]"
              >
                <p className="text-white text-[16-px]">Learn More</p>
              </Link>
            </div>
          </div>
        </div>
      </div>

      {index === items.length - 1 && (
        <Link
          href={url}
          className={`group w-full h-[250px] rounded-[24px] ${colSpan} bg-white/50 flex items-center justify-center`}
        >
          <p
            className={`${
              colCount < 1 ? "text-[26px]" : "text-[30px]"
            } transition-all font-semibold hover:scale-110`}
          >
            Discover More
          </p>
        </Link>
      )}
    </>
  );
};

const SliderItems = ({ item, color, url }: any) => {
  const bgColor = `hover:${color}`;

  return (
    <>
      <div
        className={`group ${bgColor} relative z-10 ${
          item.product === "Discover More" ? "bg-white/50" : "bg-white"
        } flex flex-col justify-center items-center rounded-xl w-[calc(100%-8px)] h-full`}
      >
        {item.product === "Discover More" ? (
          <Link href={url}>
            <p className="font-semibold text-[#404040] text-center text-[11px]">
              {item.product}
            </p>
          </Link>
        ) : (
          <Link href={item.url}>
            <div className=" flex flex-col items-center justify-center gap-2">
              {item.icon && (
                <Image
                  loader={imageLoader}
                  alt={"title"}
                  src={item.icon}
                  width={48}
                  height={48}
                />
              )}
              <p className="font-semibold text-[#404040] text-center text-[11px]">
                {item.product}
              </p>
            </div>
          </Link>
        )}
      </div>
    </>
  );
};
