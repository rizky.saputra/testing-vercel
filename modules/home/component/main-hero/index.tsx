/** @format */

import { imageLoader } from "common/utils";
import Image from "next/image";
import Link from "next/link";

const MainHero = () => {
  return (
    <>
      <DesktopMainHero />
      <MobileMainHero />
    </>
  );
};

export default MainHero;

const MobileMainHero = () => {
  return (
    <div className="sm:hidden relative w-full h-[194px]">
      <div className=" absolute w-full h-[194px] z-[-10]">
        <Image
          loader={imageLoader}
          alt={"home-banner"}
          src={"/background-image/home/homepage-banner.webp"}
          width={360}
          height={194}
          className="object-fit absolute w-[100%] h-[100%]"
          priority
        />
      </div>
      <div className="flex justify-center pt-[50px]">
        <h1 className="text-[30px] font-semibold text-white leading-[2rem] text-center">
          Transforming
          <span className="italic block text-[#07a9a0]">
            Agriculture
            <span className="text-[12px] block leading-[1rem] font-normal not-italic text-white mt-1">
              Through Internet-of-things & big data
            </span>
          </span>
        </h1>
      </div>
      <div className="flex justify-center py-[8px] mt-1">
        <Link href={"#solutionbox"} scroll={false}>
          <button className="text-white text-semibold bg-[#255855] text-[8px] py-[6px] px-2 rounded-full border-[3px] border-[#57d4b6] hover:border-[#57d4b6] hover:bg-white hover:text-black scroll-smooth">
            See our services
          </button>
        </Link>
      </div>
    </div>
  );
};

const DesktopMainHero = () => {
  return (
    <div className="hidden sm:block releative h-[600px] 2xl:h-[800px]">
      <div className=" absolute w-full h-[600px] 2xl:h-[800px] z-[-1]">
        <Image
          loader={imageLoader}
          alt={"aria-agriculture-banner"}
          src={"/background-image/home/homepage-banner.webp"}
          width={940}
          height={311}
          className="object-fit absolute w-[100%] h-[100%]"
          priority
        />
      </div>
      <div className="flex px-[100px] [@media(min-width:1700px)]:px-[300px] text-[#F6F6F6] h-full items-center">
        <div>
          <h1 className="text-[64px] leading-[5rem] text-white font-semibold">
            Transforming
            <span className="text-[#07A9A0] block italic leading-[3rem]">
              Agriculture
            </span>
          </h1>
          <p className="text-white text-[22px] mt-5 leading-[3rem]">
            Through Internet-of-things & big data
          </p>

          <div className="flex">
            <Link href={"#solutionbox"} scroll={false} className="mt-5">
              <div className="group rounded-full bg-gradient-to-r from-[#07A9A0] to-[#A9FFCC] p-[4px]">
                <div className="flex items-center justify-center bg-[#255855] back rounded-full group-hover:bg-white  px-6 py-2">
                  <h1 className="text-[24px] font-medium text-white  group-hover:text-black">
                    See our services
                  </h1>
                </div>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};
