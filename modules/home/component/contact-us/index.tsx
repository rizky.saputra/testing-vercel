/** @format */

const ContactUs = () => {
  return (
    <div className='pt-[80px] pl-[112px]'>
      <h1 className='text-[#255855] text-[36px] font-semibold'>
        Ready for transformation?
      </h1>
      <h1 className='text-[36px] text-[#404040]'>
        Start your journey with us and let's make an impact
      </h1>
      <div className='pt-4'>
        <button className='bg-[#255855] text-white py-2 px-4 rounded-full text-[16px] border-[3px] border-transparent hover:border-[#57d4b6]'>
          Contact us
        </button>
      </div>
    </div>
  );
};

export default ContactUs;
