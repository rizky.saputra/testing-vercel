import { _MILESTONES_ } from "modules/home/constant";
import CountUp from "react-countup";

const Milestone = () => {
  return (
    <div className="grid grid-cols-2 px-8 mt-4 py-5 gap-4 lg:flex lg:items-center lg:justify-between sm:px-[100px] [@media(min-width:1700px)]:px-[300px] text-[#F6F6F6] sm:my-16 sm:mt-20">
      {_MILESTONES_.map((item, index) => (
        <div className="relative" key={index}>
          <div className="absolute left-0 top-[-10px] z-[-1] w-10 h-10 bg-[#f7f3e2] rounded-full lg:w-20 lg:h-20 lg:top-[-1.8rem] lg:left-[-1.4rem]"></div>
          <p className="text-[#255855] text-2xl font-semibold px-2.5 lg:text-5xl">
            <CountUp end={item.number} />
            {item.unit && <>{item.unit}</>}
          </p>
          <p className="text-[#808080] text-xs leading-4 lg:text-base lg:leading-8">
            {item.description}
          </p>
        </div>
      ))}
    </div>
  );
};

export default Milestone;
