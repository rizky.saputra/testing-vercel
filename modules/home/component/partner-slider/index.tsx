import useWindowSize from "@/hooks/useWindowSize";
import { imageLoader } from "common/utils";
import Image from "next/image";
import { CarouselProvider, Slider, Slide } from "pure-react-carousel";
import { useEffect, useState } from "react";

const BANNER_ITEMS = [
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/fap.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/bayer.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sygenta.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sampoerna.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sag.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sinarmas.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/triputra.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/linfox.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/fap.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/bayer.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sygenta.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sampoerna.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sag.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sinarmas.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/triputra.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/linfox.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/fap.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/bayer.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sygenta.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sampoerna.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sag.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sinarmas.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/triputra.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/linfox.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/fap.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/bayer.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sygenta.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sampoerna.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sag.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sinarmas.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/triputra.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/linfox.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/fap.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/bayer.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sygenta.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sampoerna.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sag.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sinarmas.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/triputra.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/linfox.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/fap.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/bayer.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sygenta.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sampoerna.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sag.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sinarmas.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/triputra.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/linfox.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/fap.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/bayer.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sygenta.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sampoerna.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sag.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sinarmas.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/triputra.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/linfox.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/fap.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/bayer.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sygenta.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sampoerna.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sag.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/sinarmas.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/triputra.webp",
  },
  {
    alt: "aria-farmer",
    image: "/background-image/home/partner/linfox.webp",
  },
];

const PartnerSlider = () => {
  const [totalVisibleItems, setTotalVisibleItems] = useState(8);
  const { width } = useWindowSize();
  const isMobile = width < 640;

  useEffect(() => {
    if (isMobile) {
      setTotalVisibleItems(4);
    } else {
      setTotalVisibleItems(8);
    }
  }, [isMobile]);

  return (
    <CarouselProvider
      isPlaying
      interval={2000}
      className="relative"
      naturalSlideWidth={100}
      naturalSlideHeight={100}
      totalSlides={BANNER_ITEMS.length}
      visibleSlides={totalVisibleItems}
      infinite
    >
      <Slider trayTag={"div"}>
        {BANNER_ITEMS.map((item, index) => (
          <Slide key={index} index={index}>
            <div className="w-full h-full">
              <div className="w-full h-full  relative">
                <Image
                  loader={imageLoader}
                  alt={item.alt}
                  src={item.image}
                  fill
                  className="object-cover"
                  priority
                />
              </div>
            </div>
          </Slide>
        ))}
      </Slider>
    </CarouselProvider>
  );
};

export default PartnerSlider;
