import useWindowSize from "@/hooks/useWindowSize";
import { imageLoader } from "common/utils";
import Image from "next/image";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
  DotGroup,
} from "pure-react-carousel";

const BANNER_ITEMS = [
  {
    alt: "aria-farmer",
    image: "/background-image/home/farmer-farming-banner.webp",
    readMore: "",
    copyWriting: () => (
      <div className="px-6">
        <p className="font-semibold text-[18px] sm:text-[38px] xl:text-4xl lg:w-36 lg:mb-2">
          Plant {` `}
          <span className="lg:inline-block lg:mt-2 lg:font-normal">
            confidently
          </span>
        </p>
        <p className="text-xs leading-[18px] text-[12px] sm:text-[22px] lg:w-[28rem] lg:leading-8">
          Aria wants to improve Indonesian agriculture from Sabang to Merauke
          both in yield and efficiency
        </p>
      </div>
    ),
  },
  {
    alt: "aria-drone",
    image: "/background-image/home/spraying-drone-slider.webp",
    readMore: "a",
    copyWriting: () => (
      <div className="px-6">
        <p className="text-[8px] sm:text-xl">The latest on Aria’s</p>
        <p className="font-semibold text-[18px] sm:text-[38px] w-48 leading-5 mb-1 xl:text-4xl lg:w-[31rem]">
          Sustainability Development Goals
        </p>
        <p className="text-[8px] w-44 lg:text-xl lg:w-[31rem]">
          Drone Spraying for Responsible Consumption and Production
        </p>
      </div>
    ),
  },
];

const BannerSlider = () => {
  const { width } = useWindowSize();
  const isMobile = width < 640;
  return (
    <CarouselProvider
      isPlaying
      interval={8000}
      className="relative"
      naturalSlideWidth={100}
      naturalSlideHeight={isMobile ? 34 : 25}
      totalSlides={BANNER_ITEMS.length}
      infinite
    >
      <Slider trayTag={"div"}>
        {BANNER_ITEMS.map((item, index) => (
          <Slide key={index} index={index}>
            <div className="relative w-full h-full">
              <div className="flex h-full w-full items-center sm:px-[100px] [@media(min-width:1700px)]:px-[300px] lg:px-28">
                <div className="flex flex-col text-white">
                  {item.copyWriting()}
                </div>
              </div>
              {item.readMore && (
                <div className="flex flex-row items-center absolute w-full">
                  <p className="text-white mr-5">Read More</p>
                  <div className="border text-white lg:h-[25px] lg:w-[25px] shadow-sm rounded-full flex justify-center items-center">
                    {">"}
                  </div>
                </div>
              )}

              <Image
                loader={imageLoader}
                alt={"aria-industry-banner"}
                src={item.image}
                fill
                className="absolute z-[-1] top-0 w-[100%] h-[100%]"
                priority
              />
            </div>
          </Slide>
        ))}
      </Slider>
      <div className="flex absolute w-full justify-center items-center bottom-1 lg:bottom-4">
        <ButtonBack className="h-[18px] w-[18px] sm:h-4 rounded-full flex justify-center items-center lg:h-6 lg:w-6 active:scale-[1.2]">
          <Image
            loader={imageLoader}
            alt={"title"}
            src={"/logo-asset/chevron-right.svg"}
            width={50}
            height={50}
            className="rotate-180 disable-drag"
          />
        </ButtonBack>
        <DotGroup
          className="mx-2"
          dotNumbers
          renderDots={(prop) => {
            return (
              <div className="flex w-full items-center">
                {BANNER_ITEMS.map((_, index) => (
                  <div
                    key={index}
                    className={`${
                      prop.currentSlide === index
                        ? "scale-[1.2] lg:scale-[2.5]"
                        : "bg-white/50"
                    }  rounded-full mx-3 flex items-center justify-center w-2 h-2 lg:h-4 lg:w-4`}
                  >
                    {prop.currentSlide === index && (
                      <div className="bg-white rounded-full w-2 h-2 lg:h-2.5 lg:w-2.5" />
                    )}
                  </div>
                ))}
              </div>
            );
          }}
        />
        <ButtonNext className="h-[18px] w-[18px] sm:h-4 sm:w-4 rounded-full flex justify-center items-center lg:h-6 lg:w-6 active:scale-[1.2]">
          <Image
            loader={imageLoader}
            alt={"title"}
            src={"/logo-asset/chevron-right.svg"}
            width={50}
            height={50}
            className="disable-drag"
          />
        </ButtonNext>
      </div>
    </CarouselProvider>
  );
};

export default BannerSlider;
