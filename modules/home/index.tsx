/** @format */
import Head from "next/head";
import Image from "next/image";
import Milestone from "./component/milestone";
import Misi from "./component/misi";
import {
  _MILESTONES_,
  _MISI_,
  _OUR_PARTNER_,
  _OUR_SOLUTION_,
} from "./constant";
import { imageLoader } from "common/utils";
import MainHero from "./component/main-hero";
import SolutionBox from "./component/solution-box";
import ContactUs from "common/components/contact-us";
import BannerSlider from "./component/banner-slider";
import PartnerSlider from "./component/partner-slider";
import useWindowSize from "@/hooks/useWindowSize";

const HomeModule = () => {
  const { width } = useWindowSize();
  const isMobile = width < 640;

  const ImageText = (
    <p className="text-[#F6F6F6] text-xs leading-[18px] mt-2 sm:hidden md:hidden lg:hidden">
      <span className="block">Experienced</span>
      <span className="block">in the plantation and</span>
      <span className="block">forestry industry and IoT</span>
      <span className="block">business transformation</span>
    </p>
  );

  const ImageTextLg = (
    <p className="hidden text-[#F6F6F6] leading-[54px] text-4xl sm:block md:block lg:block lg:w-[600px]">
      Experienced in the plantation and forestry industry and IoT business
      transformation
    </p>
  );

  return (
    <>
      <Head>
        <title>Aria Agriculture Indonesia</title>
        <meta
          name="description"
          content="Aria Indonesia offering the tech-solution for agriculture, mining, and foresty to enchance product quality efficiently in the fastest way."
        />
        <meta
          name="keyword"
          content="Mining, Foresty, agriculture, Aria Indonesia, Aria Agriculture"
        />
        <meta property="og:title" content={"Aria Agriculture Indonesia"} />
        <meta
          property="og:description"
          content={
            "Aria Indonesia offering the tech-solution for agriculture, mining, and foresty to enchance product quality efficiently in the fastest way."
          }
        />
      </Head>
      <MainHero />
      <Milestone />
      <BannerSlider />
      <div className="flex px-6 h-full items-center mt-6 sm:px-[100px] [@media(min-width:1700px)]:px-[300px] lg:mt-20 lg:mb-14">
        <div id="solutionbox">
          <p className="text-[#255855] text-lg font-semibold lg:text-4xl">
            Our Solution for
          </p>
          <p className="text-xs text-[#404040] leading-5 lg:text-4xl lg:leading-[3rem]">
            the agriculture sector & other services
          </p>
        </div>
      </div>
      <div className="mb-6 lg:mb-24 sm:px-[100px] [@media(min-width:1700px)]:px-[300px] text-[#F6F6F6]">
        {_OUR_SOLUTION_.map((item, index) => (
          <SolutionBox key={index} {...item} />
        ))}
      </div>
      <div className="lg:mb-6">
        <div className="flex h-full items-center bg-[#fffbeb] px-6 py-3.5 sm:px-[100px] [@media(min-width:1700px)]:px-[300px] lg:py-14">
          <p className="hidden sm:block text-xs font-semibold text-[#255855] lg:text-4xl">
            Developing Sustainability Future{" "}
            <span className="text-[#404040] font-normal">with ARIA</span>
          </p>
        </div>
        <div
          className="bg-no-repeat bg-cover pt-[66.5%] relative w-full
		bg-[url(/background-image/home/sustain-able-mobile.webp)] lg:bg-[url(/background-image/home/sustain-able.webp)] lg:pt-[16%]"
        />
      </div>
      <div className="flex flex-col lg:mb-10">
        <div className="h-full mt-4 lg:mb-10 pl-6 sm:px-[100px] [@media(min-width:1700px)]:px-[300px] lg:pl-28 lg:text-4xl lg:mt-10">
          <p className="text-[#255855] font-semibold">Serving various needs</p>
          <p className="text-[#404040]">from various industries</p>
        </div>
        <PartnerSlider />
      </div>
      <div className="relative flex flex-col h-40 mt-6 mb-4 lg:h-96 lg:mb-10">
        <Image
          loader={imageLoader}
          alt={"aria-industry-banner"}
          src={
            isMobile
              ? "/background-image/home/industry-banner-mobile.webp"
              : "/background-image/home/industry-banner.webp"
          }
          width="0"
          height="0"
          className="w-[100%] h-[100%]"
          priority
        />
        <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 items-center w-full pl-6  sm:px-[100px] [@media(min-width:1700px)]:px-[300px]">
          {ImageText}
          {ImageTextLg}
        </div>
      </div>
      <Misi />
      <ContactUs color={"text-[#255855]"} />
    </>
  );
};

export default HomeModule;
