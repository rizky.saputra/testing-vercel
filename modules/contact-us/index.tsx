/** @format */

import DropDowninput from "common/components/drop-down";
import Textfield from "common/components/text-field";
import * as yup from "yup";
import { useFormik } from "formik";
import { _CONTACT_OPTIONS_ } from "./constant";
import Head from "next/head";
import axios from "axios";
import MessageModal from "common/components/message-modal";
import { useState } from "react";

import Image from "next/image";

const phoneIndonesia =
  /^(\+62 ((\d{3}([ -]\d{3,})([- ]\d{4,})?)|(\d+)))|(\(\d+\) \d+)|\d{3}( \d+)+|(\d+[ -]\d+)|\d+/i;

const validationSchema = yup.object({
  firstName: yup.string().required("First name is required"),
  lastName: yup.string().required("Last name is required"),
  email: yup.string().email().required("Enter a valid email"),
  phone: yup
    .string()
    .matches(phoneIndonesia, "Phone number is not valid")
    .matches(/^\d+$/, "Phone number is not valid")
    .required("Please input your phone number"),
  service: yup.string().required("Please choose a product or service"),
});

const ContactModule = () => {
  const [openMessage, setOpenMessage] = useState(false);
  const formik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
      service: "",
      note: "",
    } as any,
    validationSchema: validationSchema,
    onSubmit: async (values, actions) => {
      await contactUs(values);
      formik.resetForm({
        values: {
          firstName: "",
          lastName: "",
          email: "",
          phone: "",
          service: "",
          note: "",
        },
      });
      setOpenMessage(true);
      setTimeout(() => setOpenMessage(false), 3000);
    },
  });

  return (
    <>
      <Head>
        <title>Contact Us</title>
        <meta
          name="description"
          content="Let's meet our experts to help you find the best solution"
        />
        <meta
          name="keyword"
          content="Aria agriculture Indonesia, Aria Indonesia, drone spraying, drone sprayer, drone semprot, drone sprayer, Internet of Things."
        />
      </Head>
      <MessageModal
        onClickOutside={() => setOpenMessage(false)}
        open={openMessage}
      >
        <div className="flex items-center p-3 px-5">
          <div className="mr-5">
            <Image
              alt={"succsess"}
              src={"/success_icon.svg"}
              width={60}
              height={60}
              priority
            />
          </div>
          <div>
            <p>Thank You</p>
            <p className="text-[#878787] text-[12px]">
              We’ll respond to you shortly!
            </p>
          </div>
        </div>
      </MessageModal>

      <div className="p-5 sm:py-20 sm:px-[100px] [@media(min-width:1700px)]:px-[300px] [@media(min-width:2000px)]:px-[400px] bg-white">
        <div className="grid sm:grid-cols-3 sm:gap-12 w-full">
          <div className="w-full py-5 sm:block ">
            <h1 className="font-medium text-[14px] sm:text-[36px] text-[#255855]">
              Contact Aria
            </h1>
            <p className="text-[12px] sm:text-[18px] text-[#222222] ">Email:</p>
            <p className="text-[12px] sm:text-[18px] text-[#222222] underline">
              marketing@aria-operation.com
            </p>
            <p className="text-[12px] sm:text-[18px] text-[#222222] ">
              Contact:
            </p>
            <p className="text-[12px] sm:text-[18px] text-[#222222] mb-2 sm:mb-7">
              0812-8383-2031
            </p>
            <p className="text-[10px] sm:text-[14px] mb-2 sm:mb-7">
              JL Sunter Agung podomoro Blok N2, No.9-10, Sunter, RT.10/RW.11,
              Sunter Jaya, Tj. Priok, Kota Jakarta Utara, Daerah Khusus Ibukota
              Jakarta, 14350
            </p>
            <GooleMapsIframe />
          </div>
          <div className="w-full col-span-2 order-first sm:order-none">
            <div className="bg-[#F6F6F6] w-full h-max rounded-[16px] pb-5 sm:drop-shadow-lg">
              <div className="px-6 py-4 mb-4 border-b-[1px] border-[#E5EBF3] border-dashed">
                <p className="text-[14px] sm:text-[24px] text-[#404040]">
                  Contact Us
                </p>
              </div>
              <form className="px-6" noValidate onSubmit={formik.handleSubmit}>
                <Textfield
                  label={"First name"}
                  id={"firstName"}
                  placeholder={"e.g. John"}
                  name={"firstName"}
                  value={formik.values.firstName}
                  handleChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={isError(formik, "firstName")}
                  required
                />
                <Textfield
                  label={"Last Name"}
                  id={"lastName"}
                  placeholder={"e.g. Doe"}
                  name={"lastName"}
                  value={formik.values.lastName}
                  handleChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={isError(formik, "lastName")}
                  required
                />
                <Textfield
                  label={"Email Address"}
                  id={"email"}
                  placeholder={"e.g. hiaria@email.com"}
                  name={"email"}
                  value={formik.values.email}
                  handleChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={isError(formik, "email")}
                  required
                />
                <Textfield
                  label={"Phone Number"}
                  id={"phone"}
                  placeholder={"e.g. 083784758695"}
                  name={"phone"}
                  value={formik.values.phone}
                  handleChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={isError(formik, "phone")}
                  required
                />
                <DropDowninput
                  label={"Product Or Service"}
                  id={"service"}
                  options={_CONTACT_OPTIONS_}
                  name={"service"}
                  value={formik.values.service}
                  handleChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={isError(formik, "service")}
                  required
                />
                <Textfield
                  label={"Note"}
                  id={"note"}
                  placeholder={"e.g. i want to try out area mapping"}
                  name={"note"}
                  value={formik.values.note}
                  handleChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={isError(formik, "note")}
                />
                <div className="mt-6">
                  <button
                    type="submit"
                    className="bg-[#255855] hover:bg-[#386C6A] Text-white text-white py-2 w-full rounded-[25px] text-[12px] sm:text-[16px] border-[3px] border-transparent hover:border-[#57d4b6] "
                  >
                    Send
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ContactModule;

const GooleMapsIframe = () => {
  return (
    <div className="flex justify-center">
      <p className="absolute z-[-100]">Loading ....</p>
      <iframe
        className="rounded-[10px] w-full"
        loading="lazy"
        height="420"
        id="gmap_canvas"
        src="https://maps.google.com/maps?q=aria%20agri&t=&z=13&ie=UTF8&iwloc=&output=embed"
      ></iframe>
    </div>
  );
};

const isError = (formik: any, field: string) => {
  if (formik.touched[field] && formik.errors[field]) {
    return formik.errors[field];
  }
  return false;
};

// handler
const contactUs = (form: any) => {
  return axios.post(process.env.USER_SERVICE_API_URL || "", form, {
    headers: form.options?.headers,
  });
};
