export const _CONTACT_OPTIONS_ = [
  { label: "Area Mapping", value: "area-mapping" },
  { label: "Drone Spraying", value: "drone-spraying" },
  { label: "Data Platform", value: "data-plaform" },
  { label: "Digital Agronomist", value: "digital-agronomist" },
  { label: "Turbo Spreader", value: "turbo-spreader" },
  { label: "Worker Tracker", value: "worker-tracker" },
  { label: "Other", value: "other" },
];
