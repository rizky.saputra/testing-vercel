import ClientSuccsessStories from "common/components/client-succsess-story";
import ContactUs from "common/components/contact-us";
import Head from "next/head";
import Image from "next/image";
import { imageLoader } from "common/utils";
import BannerSlider from "./components/banner-slider";

const AriaAgricultureModule = () => {
  return (
    <>
      <Head>
        <title>Aria Agriculture Indonesia</title>
        <meta
          name="description"
          content="We provides drone spraying, worker tracker, Iot turbo spreader, drone mapping, data platform, digital agronomist, USV bathymetry, hydro management, and complete drone solution for mining."
        />
        <meta
          name="keyword"
          content="Mining, Foresty, agriculture, Aria Indonesia, Aria Agriculture, Internet of Things, mechanisation in agriculture, food security, Indonesian agriculture, hydro management, Pemetaan drone, Agriculture drone, drone semprot pertanian."
        />
      </Head>
      <MainHero />
      {/* <ClientSuccsessStories color="text-[#255855]" /> */}
      <ContactUs color="text-[#255855]" />
    </>
  );
};

export default AriaAgricultureModule;

const MainHero = () => {
  return (
    <>
      <div className="bg-none lg:bg-[url(/background-image/aria-tani/banner.png)] lg:bg-no-repeat lg:bg-cover lg:bg-center lg:h-[40rem]">
        <Image
          loader={imageLoader}
          alt={"aria-tani-banner"}
          src={"/background-image/aria-tani/banner.png"}
          width="0"
          height="0"
          className="absolute w-full h-52 z-[-1] lg:hidden"
          priority
        />
        <div className="flex flex-col items-center justify-center h-full gap-2 px-6 pt-12 mb-8 lg:items-start lg:pt-3 sm:px-[100px] [@media(min-width:1700px)]:px-[300px]">
          <h1 className="font-semibold text-3xl text-white lg:text-[#07A9A0] lg:text-6xl lg:text-left lg:mb-5">
            Aria <span className="text-[#07A9A0]">Agriculture</span>
          </h1>
          <p className="text-white text-xs text-justify lg:text-xl lg:w-[33rem]">
            All agriculture-related technologies aim to enhance plantation and
            forestry companies for a better yield quality and quantity.
          </p>
        </div>
      </div>
      <MainSlider />
    </>
  );
};

const MainSlider = () => {
  return (
    <div className="bg-[#255855] rounded-t-[24px] w-full pt-5 pb-8 px-6 relative lg:top-[-7rem] lg:mb-[-7rem] lg:px-0 lg:pb-28 lg:rounded-t-[60px]">
      <div className="flex flex-col h-full gap-1.5 mb-6 sm:px-[100px] [@media(min-width:1700px)]:px-[300px] lg:mb-20">
        <h2 className="font-semibold text-[#F6F6F6] text-sm lg:text-4xl lg:my-6">
          Services
        </h2>
        <p className="text-[#F6F6F6] text-xs w-[19rem] lg:text-3xl lg:w-auto">
          Discover how Aria can take you to precision agriculture by increasing
          production yield, cutting costs, and reducing the risk of harvest
          failure.
        </p>
      </div>
      <BannerSlider />
    </div>
  );
};
