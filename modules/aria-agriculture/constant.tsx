/** @format */

export const _ALL_SERVICE_: any = {
  "area-mapping": {
    meta: {
      title: "Area Mapping Service",
      description:
        "Aria drone mapping help you to monitoring plant health mapping, plantation, inventory, DTM, DSM,CHM, and surveying.",
      keyword: "drone pemetaan, pemetaan lahan, drone mapping",
    },
    title: {
      row1: "Area",
      row2: "Mapping",
    },
    banner:
      "/background-image/aria-agriculture/area-mapping/area-mapping-banner.png",
    description: {
      row1: "Capture geographic information and visuals, quick tree counting,",
      row2: "gain plant nutrition insights, and locate sick crops.",
    },
    features: {
      title: "Aria Mapping Features",
      description: "Explore our services",
      services: [
        {
          title: "Capture Layers",
          titleHover: "Capture Layers",
          icon: "/icons/aria-agriculture/area-mapping/capture-layer.svg",
          iconHover:
            "/icons/aria-agriculture/area-mapping/capture-layer-hover.svg",
          description:
            "With our drone and multispectral camera, capture data on Digital Surface Model (DSM), Digital Terrain Model (DTM), Digital Elevation Model (DEM), RGB Mosaic Map,  NDVI, Canopy Height Model, and many more. Take action to improve farmland conditions and increase harvest quantity and quality.",
        },
        {
          title: "Tree Counting & Mapping",
          titleHover: "Tree Counting",
          icon: "/icons/aria-agriculture/area-mapping/tree-counting.svg",
          iconHover:
            "/icons/aria-agriculture/area-mapping/tree-counting-hover.svg",
          description:
            "Gain insights through plant health mapping and early disease detection.",
        },
        {
          title: "Up to 1200 Ha",
          titleHover: "Up to 1200 Ha",
          icon: "/icons/aria-agriculture/area-mapping/drone-capability.svg",
          iconHover:
            "/icons/aria-agriculture/area-mapping/drone-capability-hover.svg",
          description:
            "Our drones can cover up to 1200 Ha with a flight time of 110 minutes.",
        },
        {
          title: "Fast Airborne",
          titleHover: "Fast Airborne",
          icon: "/icons/aria-agriculture/area-mapping/fast-airbone.svg",
          iconHover:
            "/icons/aria-agriculture/area-mapping/fast-airbone-hover.svg",
          description:
            "No pre-flight calibrations necessary. Get airborne in under 2 minutes.",
        },
        {
          title: "Locate & Treat Plants",
          titleHover: "Treat Plants",
          icon: "/icons/aria-agriculture/area-mapping/treat-plants.svg",
          iconHover:
            "/icons/aria-agriculture/area-mapping/treat-plants-hover.svg",
          description:
            "Detect nutrient deficient plants and take action to prevent crop failure.",
        },
        {
          title: "Crystal Clear",
          titleHover: "Crystal Clear",
          icon: "/icons/aria-agriculture/area-mapping/crystal-clear.svg",
          iconHover:
            "/icons/aria-agriculture/area-mapping/crystal-clear-hover.svg",
          description:
            "We provide quality service with high-resolution cameras up to 61MP.",
        },
        {
          title: "LIDAR Technology",
          titleHover: "LIDAR Technology",
          icon: "/icons/aria-agriculture/area-mapping/lidar-tech.svg",
          iconHover:
            "/icons/aria-agriculture/area-mapping/lidar-tech-hover.svg",
          description:
            "The latest LIDAR technology to gain insights on plant health and vigor.",
        },
      ],
    },
    outputExample: {
      title: "Drone Mapping Output Example",
      description: "",
      bgColor: "",
      outputItemExamples: [
        {
          image: "/icons/aria-agriculture/area-mapping/rgb.svg",
          title: "Red green blue (Mosaic Map)",
          description: "",
        },
        {
          image: "/icons/aria-agriculture/area-mapping/normal-dsm.svg",
          title: "Normalized difference vegetation index (NDVI)",
          description: "",
        },
        {
          image: "/icons/aria-agriculture/area-mapping/dsm.svg",
          title: "Digital surface model (DSM)",
          description: "",
        },
        {
          image: "/icons/aria-agriculture/area-mapping/dtm.svg",
          title: "Digital terrain model (DTM)",
          description: "",
        },
      ],
      outputImageExamples: [],
      bannerExample: {
        type: "",
        alt: "",
        title: "",
        image: "",
      },
    },
    servicePreview: {
      title: "Tree Density Model Report",
      backgroundImage:
        "/background-image/aria-agriculture/area-mapping/area-mapping-preview-banner.png",
      image:
        "/background-image/aria-agriculture/area-mapping/area-mapping-preview.png",
    },
  },
  "drone-spraying": {
    meta: {
      title: "Aria Drone Spraying",
      description:
        "Aria drone spraying help planting process more efficiency and faster.",
      keyword:
        "drone spraying, aria drone spraying, Alat semprot pertanian, Drone sprayer, Drone semprot pertanian",
    },
    title: {
      row1: "Drone",
      row2: "Spraying",
    },
    banner:
      "/background-image/aria-agriculture/drone-spraying/drone-spraying-banner.png",
    description: {
      row1: "Autonomous Drone Spraying to swarm economic efficiency.",
      row2: "",
    },
    features: {
      title: "Drone Spraying Features",
      description: "Explore our services",
      services: [
        {
          title: "Improve Fertilizing Efficiency",
          titleHover: "Improve Fertilizing Efficiency",
          icon: "/icons/aria-agriculture/drone-spraying/improve-fertilizing-efficiency.svg",
          iconHover:
            "/icons/aria-agriculture/drone-spraying/improve-fertilizing-efficiency-hover.svg",
          description:
            "With 20 L detachable tank, our drone can achieve efficiency by maximizing amount of area coverage per flight.",
        },
        {
          title: "Blanket Spraying",
          titleHover: "Blanket Spraying",
          icon: "/icons/aria-agriculture/drone-spraying/blanket-spraying.svg",
          iconHover:
            "/icons/aria-agriculture/drone-spraying/blanket-spraying-hover.svg",
          description:
            "Eliminate guesswork and inconsistent fertilizer usage, monitor resource output, and cut costs.",
        },
        {
          title: "Selective Spraying",
          titleHover: "Selective Spraying",
          icon: "/icons/aria-agriculture/drone-spraying/selective-spraying.svg",
          iconHover:
            "/icons/aria-agriculture/drone-spraying/selective-spraying-hover.svg",
          description:
            "Reduce chemical waste by spraying only on nutrient deficient crops.",
        },
        {
          title: "Fast Airborne",
          titleHover: "Fast Airborne",
          icon: "/icons/aria-agriculture/drone-spraying/fast-airbone.svg",
          iconHover:
            "/icons/aria-agriculture/drone-spraying/fast-airbone-hover.svg",
          description:
            "No pre-flight calibrations necessary. Get airborne in under 2 minutes.",
        },
        {
          title: "Locate & Treat Plants",
          titleHover: "Treat Plants",
          icon: "/icons/aria-agriculture/drone-spraying/locate-treat-plants.svg",
          iconHover:
            "/icons/aria-agriculture/drone-spraying/locate-treat-plants-hover.svg",
          description:
            "Detect nutrient deficient plants and take action to prevent crop failure.",
        },
        {
          title: "Increased Flight Safety",
          titleHover: "Flight Safety",
          icon: "/icons/aria-agriculture/drone-spraying/increased-flight-safety.svg",
          iconHover:
            "/icons/aria-agriculture/drone-spraying/increased-flight-safety-hover.svg",
          description:
            "Our drones can safely avoid people, animals, utility poles, vehicles, and other drones.",
        },
        {
          title: "Automated Data",
          titleHover: "Automated Data",
          icon: "/icons/aria-agriculture/drone-spraying/automated-data.svg",
          iconHover:
            "/icons/aria-agriculture/drone-spraying/automated-data-hover.svg",
          description: "Our drones can complete pre-programmed jobs in tandem.",
        },
      ],
    },
    outputExample: {
      title: "Drone Spraying Use Case: Spot Spraying",
      description: "",
      bgColor: "",
      outputItemExamples: [
        {
          image: "/icons/aria-agriculture/drone-spraying/aerial-photo.svg",
          title: "Aerial Photo",
          description: "Detect field map, nutrient deficiencies and pest",
        },
        {
          image:
            "/icons/aria-agriculture/drone-spraying/flight-mission-planning.svg",
          title: "Flight Mission Planning",
          description: "Create flight plan based on Aerial Photo insight",
        },
        {
          image: "/icons/aria-agriculture/drone-spraying/plant-analysis.svg",
          title: "Plant Analysis",
          description: "Plot specific crops that need fertilizing or pesticide",
        },
        {
          image:
            "/icons/aria-agriculture/drone-spraying/flight-implementation.svg",
          title: "Flight Implementation",
          description: "Automatic drone spraying on critical crops",
        },
      ],
      outputImageExamples: [],
      bannerExample: {
        type: "",
        alt: "",
        title: "",
        image: "",
      },
    },
    servicePreview: {
      title: "Drone Spraying Flight Report",
      backgroundImage:
        "/background-image/aria-agriculture/drone-spraying/drone-spraying-preview-bg.png",
      image:
        "/background-image/aria-agriculture/drone-spraying/drone-spraying-preview.png",
    },
  },
  "data-platform": {
    meta: {
      title: "Aria Data Platform ",
      description:
        "Our data platform in able to gather Geospatial Information System, GPS tracking, and data visualization services with accuracy data anyltics.",
      keyword:
        "agriculture technology, asset tracking, asset management, area mapping, fixed-wing, agriculture drone mapping, layanan pemetaan perkebunan, geospatial information system",
    },
    title: {
      row1: "Data",
      row2: "Platform",
    },
    banner:
      "/background-image/aria-agriculture/data-platform/data-platform-banner.png",
    description: {
      row1: "All in one platform to manage and gain insight from Aria’s services. View",
      row2: "Area Mapping visuals, plant nutrition results and predictions, fertilizer",
      row3: "recommendations, and many more.",
    },
    features: {
      title: "Data Platform Features",
      description: "Explore our services",
      services: [
        {
          title: "Data Visualization",
          titleHover: "Data Visualization",
          icon: "/icons/aria-agriculture/data-platform/data-visualization-icon.svg",
          iconHover:
            "/icons/aria-agriculture/data-platform/data-visualization-icon-hover.svg",
          description:
            "View all reports and visuals from Area Mapping, Drone Spraying, Worker Tracker, Digital Agronomist, and Turbo Spreader. View flight reports, data insights, see real-time reports, and many more. ",
        },
        {
          title: "Data Analytics",
          titleHover: "Data Analytics",
          icon: "/icons/aria-agriculture/data-platform/data-analytics-icon.svg",
          iconHover:
            "/icons/aria-agriculture/data-platform/data-analytics-icon-hover.svg",
          description:
            "Access analyzed data on dashboard for better insight and decision making.",
        },
        {
          title: "Fertilizer Recommendations",
          titleHover: "Recommendations",
          icon: "/icons/aria-agriculture/data-platform/fertilizer-recommendations-icon.svg",
          iconHover:
            "/icons/aria-agriculture/data-platform/fertilizer-recommendations-icon-hover.svg",
          description:
            "View nutrient reports and fertilizer or pesticide recommendations.",
        },
        {
          title: "IoT Integration",
          titleHover: "IoT Integration",
          icon: "/icons/aria-agriculture/data-platform/iot-integration-icon.svg",
          iconHover:
            "/icons/aria-agriculture/data-platform/iot-integration-icon-hover.svg",
          description:
            "Connecting IoT devices and Aria Data Platform for real-time reports.",
        },
        {
          title: "Collaboration at The Core",
          titleHover: "Collaboration",
          icon: "/icons/aria-agriculture/data-platform/collaboration-at-the-core-icon.svg",
          iconHover:
            "/icons/aria-agriculture/data-platform/collaboration-at-the-core-icon-hover.svg",
          description:
            "Multiple users can interact within the system for streamlining the business.",
        },
        {
          title: "Output Report",
          titleHover: "Output Report",
          icon: "/icons/aria-agriculture/data-platform/output-report-icon.svg",
          iconHover:
            "/icons/aria-agriculture/data-platform/output-report-icon-hover.svg",
          description:
            "View reports from Aria’s Services within Data Platform.",
        },
        {
          title: "Actionable Insights",
          titleHover: "Actionable Insights",
          icon: "/icons/aria-agriculture/data-platform/actionable-insights-icon.svg",
          iconHover:
            "/icons/aria-agriculture/data-platform/actionable-insights-icon-hover.svg",
          description:
            "Improve decision making through data insights and agronomy recommendations.",
        },
      ],
    },
    outputExample: {
      title: "Drone Spraying on Data Platform",
      description: "",
      bgColor: "",
      outputItemExamples: [],
      outputImageExamples: [
        {
          alt: "data-platform-preview",
          image:
            "/background-image/aria-agriculture/data-platform/data-platform-preview.png",
        },
      ],
      bannerExample: {
        type: "",
        alt: "",
        title: "",
        image: "",
      },
    },
    servicePreview: {
      title: "Data Platform Dashboard",
      backgroundImage:
        "/background-image/aria-agriculture/data-platform/data-platform-bg.png",
      image:
        "/background-image/aria-agriculture/data-platform/data-platform-dashboard.png",
    },
  },
  "digital-agronomist": {
    meta: {
      title: "Digital Agronomist",
      description: "Measure and monitoring your plant health with our expert.",
      keyword:
        "digital agronomist, agronomis, agronomist, kesehatan tanaman, tanaman sehat, gagal panen",
    },
    title: {
      row1: "Digital",
      row2: "Agronomist",
    },
    banner:
      "/background-image/aria-agriculture/digital-agronomist/digital-agronomist-banner.png",
    mobileBanner:
      "/background-image/aria-agriculture/digital-agronomist/digital-agronomist-banner-mobile.png",
    description: {
      row1: "Measure and predict levels of macronutrients (N, P, K) using Agronomy,",
      row2: "Drone and Satellite Imagery, and Machine Learning.",
    },
    features: {
      title: "Digital Agronomist Features",
      description: "Explore our services",
      services: [
        {
          title: "Machine Learning Model",
          titleHover: "Machine Learning Model",
          icon: "/icons/aria-agriculture/digital-agronomist/machine-learning.svg",
          iconHover:
            "/icons/aria-agriculture/digital-agronomist/machine-learning-hover.svg",
          description:
            "Machine learning model with insights from multispectral camera data, satellite data, and agronomy. Measure and predict levels of macronutrients (N, P, K), learn from powerful predictions.",
        },
        {
          title: "Tree Counting & Mapping",
          titleHover: "Tree Counting",
          icon: "/icons/aria-agriculture/digital-agronomist/tree-counting.svg",
          iconHover:
            "/icons/aria-agriculture/digital-agronomist/tree-counting-hover.svg",
          description:
            "Gain insights through plant health mapping and early disease detection.",
        },
        {
          title: "Multispectral Camera",
          titleHover: "Multispectral",
          icon: "/icons/aria-agriculture/digital-agronomist/multispectral-camera.svg",
          iconHover:
            "/icons/aria-agriculture/digital-agronomist/multispectral-camera-hover.svg",
          description:
            "Classify areas with sick plants through multispectral aerial photos.",
        },
        {
          title: "Satellite Imagery",
          titleHover: "Satellite Imagery",
          icon: "/icons/aria-agriculture/digital-agronomist/satellite-imagery.svg",
          iconHover:
            "/icons/aria-agriculture/digital-agronomist/satellite-imagery-hover.svg",
          description:
            "View nutrition maps from satellite imagery. Take action to treat sick plants.",
        },
        {
          title: "Locate & Treat Plants",
          titleHover: "Treat Plants",
          icon: "/icons/aria-agriculture/area-mapping/treat-plants.svg",
          iconHover:
            "/icons/aria-agriculture/area-mapping/treat-plants-hover.svg",
          description:
            "Detect nutrient deficient plants and take action to prevent crop failure.",
        },
        {
          title: "Predict Nutrient Content",
          titleHover: "Nutrient Content",
          icon: "/icons/aria-agriculture/digital-agronomist/predict-nutrient-content.svg",
          iconHover:
            "/icons/aria-agriculture/digital-agronomist/predict-nutrient-content-hover.svg",
          description:
            "Measure and predict nutrient content through satellite imagery and machine learning model.",
        },
        {
          title: "Output Report",
          titleHover: "Output Report",
          icon: "/icons/aria-agriculture/digital-agronomist/output-report.svg",
          iconHover:
            "/icons/aria-agriculture/digital-agronomist/output-report-hover.svg",
          description:
            "Detailed output report with digestible data insights and actionable item recommendations.",
        },
      ],
    },
    outputExample: {
      title: "Nutrition Map Example",
      description: "",
      bgColor: "bg-white",
      mobileImage: {
        alt: "nutrition-example",
        image:
          "/background-image/aria-agriculture/digital-agronomist/nutrition-example-mobile.png",
      },
      outputItemExamples: [],
      outputImageExamples: [
        {
          alt: "nutrition-example",
          image:
            "/background-image/aria-agriculture/digital-agronomist/nutrition-example.png",
        },
      ],
      bannerExample: {
        type: "",
        alt: "",
        title: "",
        image: "",
      },
    },
    servicePreview: {
      title: "Nutrition Detection on ARIA Tani App",
      backgroundImage:
        "/background-image/aria-agriculture/digital-agronomist/agro-preview-bg.png",
      image:
        "/background-image/aria-agriculture/digital-agronomist/agro-preview.png",
    },
  },
  "turbo-spreader": {
    meta: {
      title: "Aria Iot Turbo Spreader",
      description:
        "Aria IoT Turbo Spreader offering an easy and flexible method to measure the fertilizer.",
      keyword:
        "pertanian digital, Aria turbo spreader, proses pemupukan, penyebaran biji",
    },
    title: {
      row1: "",
      row2: "Turbo Spreader",
    },
    banner:
      "/background-image/aria-agriculture/turbo-spreader/turbo-spreader-banner.png",
    description: {
      row1: "A combination of IoT and data visualization for mechanical ",
      row2: "fertilizing, detecting chemical usage, conducting precision",
      row3: "spraying, and analyzing data on Aria Data Platform.",
    },
    features: {
      title: "IoT Turbo Spreader Features",
      description: "Explore our services",
      services: [
        {
          title: "IoT Integrations",
          titleHover: "IoT Integrations",
          icon: "/icons/aria-agriculture/turbo-spreader/iot-integration-icon.svg",
          iconHover:
            "/icons/aria-agriculture/turbo-spreader/iot-integration-icon-hover.svg",
          description:
            "Connecting IoT devices for mechanical fertilizing, detecting chemical usage, conducting precision spraying, locating and tracking fertilizer implementation, and ensuring the right amount of dosage was applied.",
        },
        {
          title: "Data Visualization",
          titleHover: "Data Visualization",
          icon: "/icons/aria-agriculture/turbo-spreader/data-visualization-icon.svg",
          iconHover:
            "/icons/aria-agriculture/turbo-spreader/data-visualization-icon-hover.svg",
          description: "Track turbo spreader movements and chemical usage. ",
        },
        {
          title: "Data Analysis",
          titleHover: "Data Analysis",
          icon: "/icons/aria-agriculture/turbo-spreader/data-analytics-icon.svg",
          iconHover:
            "/icons/aria-agriculture/turbo-spreader/data-analytics-icon-hover.svg",
          description:
            "Access analyzed data on dashboard for better insight and decision making.",
        },
        {
          title: "Fertilizer Dosage Monitoring",
          titleHover: "Fertilizer Dosage Monitoring",
          icon: "/icons/aria-agriculture/turbo-spreader/fertilizer-dosage-monitoring-icon.svg",
          iconHover:
            "/icons/aria-agriculture/turbo-spreader/fertilizer-dosage-monitoring-icon-hover.svg",
          description: "Detect chemical usage with precision up to 90%.",
        },
        {
          title: "GPS Integrated",
          titleHover: "GPS Integrated",
          icon: "/icons/aria-agriculture/turbo-spreader/gps-integrated-icon.svg",
          iconHover:
            "/icons/aria-agriculture/turbo-spreader/gps-integrated-icon-hover.svg",
          description:
            "The latest GPS Module with compass, integrated tuned ceramic antenna, and genuine UBlox GPS chip.",
        },
        {
          title: "Track Fertilizer Implementation",
          titleHover: "Fertilizer Tracker",
          icon: "/icons/aria-agriculture/turbo-spreader/track-fertilizer-implementation-icon.svg",
          iconHover:
            "/icons/aria-agriculture/turbo-spreader/track-fertilizer-implementation-icon-hover.svg",
          description:
            "Track chemical usage, locate fertilizer implementation, and ensure dosage outputs.",
        },
        {
          title: "Automated Data",
          titleHover: "Automated Data",
          icon: "/icons/aria-agriculture/turbo-spreader/automated-data-icon.svg",
          iconHover:
            "/icons/aria-agriculture/turbo-spreader/automated-data-icon-hover.svg",
          description: "Complete pre-programmed fertilizing jobs. ",
        },
      ],
    },
    outputExample: {
      title: "",
      description: "",
      bgColor: "",
      outputItemExamples: [],
      outputImageExamples: [],
      bannerExample: {
        type: "withMiddleTitle",
        alt: "tracktor",
        title: "Turbo Spreader Device",
        image:
          "/background-image/aria-agriculture/turbo-spreader/turbo-spreader-bg.png",
      },
    },
    servicePreview: {
      title: "Turbo Spreader Fertilizing Report",
      backgroundImage:
        "/background-image/aria-agriculture/turbo-spreader/turbo-spreader-bg-banner.png",
      image:
        "/background-image/aria-agriculture/turbo-spreader/turbo-spreader-preview.png",
    },
  },
  "worker-tracker": {
    meta: {
      title: "Aria Worker Tracker",
      description:
        "Empowering decision-makers at every level of management to meet the working area coverage. Speed, scale, and transform.",
      keyword:
        "PT. Aria Agri Indonesia, Worker Tracker, Activity Tracker, Internet of Things, IoT Device, IoT Worker Tracker, GPS Employee",
    },
    title: {
      row1: "Worker",
      row2: "Tracker",
    },
    banner:
      "/background-image/aria-agriculture/worker-tracker/worker-tracker-banner.png",
    description: {
      row1: "Keep track of work orders and harvests through insights such as tree",
      row2: "coverage per employee, map coverage, total distance covered, and",
      row3: "worker performance.",
    },
    features: {
      title: "Worker Tracker Features",
      description: "Explore our services",
      services: [
        {
          title: "GPS Integrated",
          titleHover: "GPS Integrated",
          icon: "/icons/aria-agriculture/worker-tracker/gps-integrated-icon.svg",
          iconHover:
            "/icons/aria-agriculture/worker-tracker/gps-integrated-icon-hover.svg",
          description:
            "The latest GPS module with built in compass, ideal for plantation and agriculture use cases. The device features Magnetometer (compass), integrated tuned ceramic antenna and a genuine UBlox GPS chip. Work well under a high-density canopy.",
        },
        {
          title: "Track Employee & Field Work",
          titleHover: "Track Field Work",
          icon: "/icons/aria-agriculture/worker-tracker/track-employee-field-work-icon.svg",
          iconHover:
            "/icons/aria-agriculture/worker-tracker/track-employee-field-work-icon-hover.svg",
          description:
            "Keep track of area coverage, harvesting and fertilizing sessions, and worker performance.",
        },
        {
          title: "Works Well in Harsh Field Conditions",
          titleHover: "Harsh Conditions",
          icon: "/icons/aria-agriculture/worker-tracker/works-well-in-harsh-icon.svg",
          iconHover:
            "/icons/aria-agriculture/worker-tracker/works-well-in-harsh-icon-hover.svg",
          description:
            "Proven to work well under high-intensity working behavior and under harsh field conditions. ",
        },
        {
          title: "Battery Durability",
          titleHover: "Battery Durability",
          icon: "/icons/aria-agriculture/worker-tracker/battrey-durability-icon.svg",
          iconHover:
            "/icons/aria-agriculture/worker-tracker/battrey-durability-icon-hover.svg",
          description:
            "Worker Tracker battery can last up to 20 hours and can fully charge in under 4 hours.",
        },
        {
          title: "Connectivity to Data Platform",
          titleHover: "Data Platform",
          icon: "/icons/aria-agriculture/worker-tracker/connectivity-to-data-platform-icon.svg",
          iconHover:
            "/icons/aria-agriculture/worker-tracker/connectivity-to-data-platform-icon-hover.svg",
          description:
            "Seamless data transfer to Aria Data Platform. Dashboard access to visuals and analytics.",
        },
        {
          title: "Data Visualization",
          titleHover: "Data Visualization",
          icon: "/icons/aria-agriculture/worker-tracker/data-visualization-icon.svg",
          iconHover:
            "/icons/aria-agriculture/worker-tracker/data-visualization-icon-hover.svg",
          description: "Track employee movements and area coverage.",
        },
        {
          title: "Data Analysis",
          titleHover: "Data Analysis",
          icon: "/icons/aria-agriculture/worker-tracker/data-analytics-icon.svg",
          iconHover:
            "/icons/aria-agriculture/worker-tracker/data-analytics-icon-hover.svg",
          description:
            "Gain insights on tree coverage per employee, total distance covered, and worker performance.",
        },
      ],
    },
    outputExample: {
      title: "",
      description: "",
      bgColor: "bg-white",
      outputItemExamples: [],
      outputImageExamples: [],
      bannerExample: {
        type: "withTopTitle",
        alt: "worker-tracker-device",
        title: "Worker Tracker Device",
        image:
          "/background-image/aria-agriculture/worker-tracker/worker-tracker-example.png",
      },
    },
    servicePreview: {
      title: "Worker Tracker Report",
      backgroundImage:
        "/background-image/aria-agriculture/worker-tracker/worker-tracker-bg-banner.png",
      image:
        "/background-image/aria-agriculture/worker-tracker/worker-tracker-preview.png",
    },
  },
};
