import { imageLoader } from "common/utils";
import Link from "next/link";
import Image from "next/image";
import { ButtonNext, WithStore } from "pure-react-carousel";
import { FC } from "react";

const CarousellItem: FC<any> = ({
  title,
  href,
  desc,
  image,
  currentSlide,
  index,
}: any) => {
  return (
    <div className={`relative h-full w-full px-10`}>
      {currentSlide === index && (
        <ButtonNext className="absolute top-[40%] left-[89%] border text-transparent bg-white h-[80px] w-[80px] shadow-sm rounded-full flex justify-center items-center z-20 pointer-crusor">
          <p className="text-gray-500 text-[40px]">{">"}</p>
        </ButtonNext>
      )}

      <Link href={`/aria-agriculture/${href}`}>
        <div className="relative group flex h-full w-full items-end">
          <Image
            loader={imageLoader}
            alt={title}
            src={image}
            width={940}
            height={311}
            className="absolute w-[100%] h-[100%]"
            priority
          />

          <div className="p-10 z-10">
            <p className="font-bold text-[18px] group-hover:text-[#1B974E] text-[#F6F6F6]">
              {title}
            </p>
            <p className="text-[14px] text-[#F6F6F6]">{desc}</p>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default WithStore(CarousellItem, ({ currentSlide }: any) => ({
  currentSlide: currentSlide,
}));
