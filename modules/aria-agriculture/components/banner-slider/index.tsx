import { imageLoader } from "common/utils";
import Image from "next/image";
import Link from "next/link";
import { useCallback, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Swiper as SwiperClass } from "swiper/types";
import "swiper/css";

const BANNER_ITEMS = [
  {
    title: "Area Mapping",
    href: "area-mapping",
    image: "/background-image/aria-tani/area-mapping.png",
    desc: "Capture geographic information and visuals, quick tree counting, gain plant nutrition insights, and locate sick crops using ARIA’s drone and Multispectral camera technology.",
  },
  {
    title: "Drone Spraying",
    href: "drone-spraying",
    image: "/background-image/aria-tani/drone-spraying.png",
    desc: "Multispectral camera technology Precision farming using Aria’s drone spraying service. Save on labor and fertilizer costs through quick blanket spraying or treat only sick and nutrient deficient plants..",
  },
  {
    title: "Worker Tracker",
    href: "worker-tracker",
    image: "/background-image/aria-agriculture/worker-tracker.png",
    desc: "Keep track of area covered by your employees for efficient work and harvest monitoring. Track movements and area coverage on Aria Data Platform.",
  },
  {
    title: "Data Platform",
    href: "data-platform",
    image: "/background-image/aria-agriculture/data-platform.png",
    desc: "All in one platform to manage and gain insight from Aria’s services. View Area Mapping visuals, plant nutrition results and predictions, fertilizer recommendations, and many more.",
  },
  {
    title: "Digital Agronomist",
    href: "digital-agronomist",
    image: "/background-image/aria-tani/Digital Agronomist.png",
    desc: "A combination of GIS, Agronomy, and Machine Learning, Aria’s Digital Agronomist regularly measures macronutrient levels (N, P, K) per tree/block to provide fertilizer recommendations.",
  },
  {
    title: "Turbo Spreader",
    href: "turbo-spreader",
    image: "/background-image/aria-agriculture/turbo-spreader.png",
    desc: "A combination of IoT and data visualization for mechanical fertilizing, detecting chemical usage, conducting precision spraying, and analyzing data on Aria Data Platform.",
  },
];

const BannerSlider = () => {
  const [swiperInstance, setSwiperInstance] = useState<SwiperClass>();

  const handleNext = useCallback(() => {
    swiperInstance?.slideNext();
  }, [swiperInstance]);

  return (
    <div className="relative">
      <Swiper
        onSwiper={(swiper) => setSwiperInstance(swiper)}
        loop
        spaceBetween={30}
        centeredSlides
        breakpoints={{
          640: {
            slidesPerView: 2,
          },
          1024: {
            slidesPerView: 2.4,
            spaceBetween: 80,
          },
        }}
      >
        {BANNER_ITEMS.map(({ href, title, image, desc }, idx) => {
          return (
            <SwiperSlide key={idx}>
              <Link href={`/aria-agriculture/${href}`}>
                <div className="group flex h-52 w-full p-7 lg:h-[22.25rem] 2xl:h-[28.25rem]">
                  <Image
                    loader={imageLoader}
                    alt={title}
                    src={image}
                    fill
                    priority
                  />
                  <div className="z-10 flex flex-col justify-end sm:p-5">
                    <p className="font-bold text-[14px] lg:text-[18px] group-hover:text-[#1B974E] text-[#F6F6F6]">
                      {title}
                    </p>
                    <p className="text-[12px] lg:text-[14px] text-[#F6F6F6]">
                      {desc}
                    </p>
                  </div>
                </div>
              </Link>
            </SwiperSlide>
          );
        })}
      </Swiper>
      <div
        className="sm:hidden md:hidden lg:flex absolute top-[40%] left-[92%] border text-transparent bg-white h-11 w-11
        shadow-sm rounded-full flex justify-center items-center z-10 active:opacity-0 lg:w-16 lg:h-16 lg:left-[67%]"
      >
        <button
          className="text-gray-500 text-4xl lg:text-6xl"
          onClick={handleNext}
        >
          {">"}
        </button>
      </div>
    </div>
  );
};

export default BannerSlider;
