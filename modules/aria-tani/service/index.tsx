import ProductsPageTemplate from "common/components/products-page-template";

const ServiceModule = (props: any) => {
  return <ProductsPageTemplate {...props} />;
};

export default ServiceModule;
