import ClientSuccsessStories from "common/components/client-succsess-story";
import ContactUs from "common/components/contact-us";
import Head from "next/head";
import Image from "next/image";
import { imageLoader } from "common/utils";
import BannerSlider from "./components/banner-slider";

const AriaTaniModule = () => {
  return (
    <>
      <Head>
        <title>Aria Tani Application</title>
        <meta
          name="description"
          content="An fastest way to order our drone services in your area"
        />
        <meta
          name="keyword"
          content="aria tani, aplikasi aria tani, satujariuntuktani, kelola lahan, drone spraying, drone semprot, pertanian digital"
        />
      </Head>
      <MainHero />
      {/* <ClientSuccsessStories color="text-[#1B974E]" /> */}
      <ContactUs color="text-[#1B974E]" buttonColor={"bg-[#1B974E]"} />
    </>
  );
};

export default AriaTaniModule;

const MainHero = () => {
  return (
    <>
      <div className="bg-none lg:bg-[url(/background-image/aria-tani/banner.png)] lg:bg-no-repeat lg:bg-cover lg:bg-center lg:h-[40rem]">
        <Image
          loader={imageLoader}
          alt={"aria-tani-banner"}
          src={"/background-image/aria-tani/banner.png"}
          width="0"
          height="0"
          className="absolute w-full h-52 z-[-1] lg:hidden"
          priority
        />
        <div className="flex flex-col items-center justify-center h-full gap-2 px-6 pt-12 mb-8 lg:items-start lg:pt-3 sm:px-[100px] [@media(min-width:1700px)]:px-[300px]">
          <h1 className="text-white font-semibold text-3xl leading-none text-center lg:text-6xl lg:text-left lg:mb-5">
            Aria Tani
            <span className="text-[#1B974E] font-bold block  lg:leading-tight">
              Application
            </span>
          </h1>
          <p className="text-white text-xs text-justify lg:text-xl lg:w-[38rem]">
            Order our Drone Mapping and Spraying and get fertilizer
            recommendations from Aria Tani App.
          </p>
        </div>
      </div>
      <MainSlider />
    </>
  );
};

const MainSlider = () => {
  return (
    <div className="bg-[#1B974E] rounded-t-[24px] w-full pt-5 pb-8 px-6 relative lg:top-[-7rem] lg:mb-[-7rem] lg:px-0 lg:pb-28 lg:rounded-t-[60px]">
      <div className="flex flex-col h-full gap-1.5 mb-6 sm:px-[100px] [@media(min-width:1700px)]:px-[300px] lg:mb-20">
        <h2 className="font-semibold text-[#F6F6F6] text-sm lg:text-4xl lg:my-6">
          Services
        </h2>
        <p className="text-[#F6F6F6] text-xs w-[19rem] lg:text-3xl lg:w-auto">
          Order agriculture services from Aria Tani, track drone pilot location
          and progress, and receive in-app flight reports and agronomy insights.
        </p>
      </div>
      <BannerSlider />
    </div>
  );
};
