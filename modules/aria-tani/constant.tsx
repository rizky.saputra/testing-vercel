/** @format */

export const _ALL_SERVICE_: any = {
  "area-mapping": {
    meta: {
      title: "Area Mapping Service",
      description:
        "Aria drone mapping help you to monitoring plant health mapping, plantation, inventory, DTM, DSM,CHM, and surveying.",
      keyword: "drone pemetaan, pemetaan lahan, drone mapping",
    },
    title: {
      row1: "Area",
      row2: "Mapping",
    },
    banner: "/background-image/aria-tani/area-mapping/area-mapping-banner.png",
    description: {
      row1: "Capture geographic information and visuals, quick tree counting,",
      row2: "gain plant nutrition insights, and locate sick crops.",
    },
    features: {
      title: "Aria Mapping Features",
      description: "Explore our services",
      services: [
        {
          title: "Capture Layers",
          icon: "/icons/aria-tani/area-mapping/capture-layer.svg",
          iconHover: "/icons/aria-tani/area-mapping/capture-layer-hover.svg",
          description:
            "With our drone and multispectral camera, capture data on Digital Surface Model (DSM), Digital Terrain Model (DTM), Digital Elevation Model (DEM), RGB Mosaic Map,  NDVI, Canopy Height Model, and many more. Take action to improve farmland conditions and increase harvest quantity and quality.",
        },
        {
          title: "Tree Counting & Mapping",
          icon: "/icons/aria-tani/area-mapping/tree-counting.svg",
          iconHover: "/icons/aria-tani/area-mapping/tree-counting-hover.svg",
          description:
            "Gain insights through plant health mapping and early disease detection.",
        },
        {
          title: "Up to 1200 Ha",
          icon: "/icons/aria-tani/area-mapping/drone-capability.svg",
          iconHover: "/icons/aria-tani/area-mapping/drone-capability-hover.svg",
          description:
            "Our drones can cover up to 1200 Ha with a flight time of 110 minutes.",
        },
        {
          title: "Fast Airborne",
          icon: "/icons/aria-tani/area-mapping/fast-airbone.svg",
          iconHover: "/icons/aria-tani/area-mapping/fast-airbone-hover.svg",
          description:
            "No pre-flight calibrations necessary. Get airborne in under 2 minutes.",
        },
        {
          title: "Locate & Treat Plants",
          icon: "/icons/aria-tani/area-mapping/treat-plants.svg",
          iconHover: "/icons/aria-tani/area-mapping/treat-plants-hover.svg",
          description:
            "Detect nutrient deficient plants and take action to prevent crop failure.",
        },
        {
          title: "Crystal Clear",
          icon: "/icons/aria-tani/area-mapping/crystal-clear.svg",
          iconHover: "/icons/aria-tani/area-mapping/crystal-clear-hover.svg",
          description:
            "We provide quality service with high-resolution cameras up to 61MP.",
        },
        {
          title: "LIDAR Technology",
          icon: "/icons/aria-tani/area-mapping/lidar-tech.svg",
          iconHover: "/icons/aria-tani/area-mapping/lidar-tech-hover.svg",
          description:
            "The latest LIDAR technology to gain insights on plant health and vigor.",
        },
      ],
    },
    outputExample: {
      title: "Drone Mapping Output Example",
      description: "",
      outputItemExamples: [
        {
          image: "/icons/aria-tani/area-mapping/rgb.svg",
          title: "Red green blue (Mosaic Map)",
          description: "",
        },
        {
          image: "/icons/aria-tani/area-mapping/normal-dsm.svg",
          title: "Normalized difference vegetation index (NDVI)",
          description: "",
        },
        {
          image: "/icons/aria-tani/area-mapping/dsm.svg",
          title: "Digital surface model (DSM)",
          description: "",
        },
        {
          image: "/icons/aria-tani/area-mapping/dtm.svg",
          title: "Digital terrain model (DTM)",
          description: "",
        },
      ],
      outputImageExamples: [],
      bannerExample: {
        type: "",
        alt: "",
        title: "",
        image: "",
      },
    },
    servicePreview: {
      title: "Tree Density Model Report",
      backgroundImage:
        "/background-image/aria-tani/area-mapping/tree-density-preview-bg.png",
      image:
        "/background-image/aria-tani/area-mapping/tree-density-preview.png",
    },
  },
  "drone-spraying": {
    meta: {
      title: "Aria Drone Spraying",
      description:
        "Aria drone spraying help planting process more efficiency and faster.",
      keyword:
        "drone spraying, aria drone spraying, Alat semprot pertanian, Drone sprayer, Drone semprot pertanian",
    },
    title: {
      row1: "Drone",
      row2: "Spraying",
    },
    banner:
      "/background-image/aria-tani/drone-spraying/drone-spraying-banner.png",
    description: {
      row1: "Autonomous Drone Spraying to swarm economic efficiency.",
      row2: "",
    },
    features: {
      title: "Drone Spraying Features",
      description: "Explore our services",
      services: [
        {
          title: "Improve Fertilizing Efficiency",
          icon: "/icons/aria-tani/drone-spraying/improve-fertilizing-efficiency.svg",
          iconHover:
            "/icons/aria-tani/drone-spraying/improve-fertilizing-efficiency-hover.svg",
          description:
            "With 20 L detachable tank, our drone can achieve efficiency by maximizing amount of area coverage per flight.",
        },
        {
          title: "Blanket Spraying",
          icon: "/icons/aria-tani/drone-spraying/blanket-spraying.svg",
          iconHover:
            "/icons/aria-tani/drone-spraying/blanket-spraying-hover.svg",
          description:
            "Eliminate guesswork and inconsistent fertilizer usage, monitor resource output, and cut costs.",
        },
        {
          title: "Selective Spraying",
          icon: "/icons/aria-tani/drone-spraying/selective-spraying.svg",
          iconHover:
            "/icons/aria-tani/drone-spraying/selective-spraying-hover.svg",
          description:
            "Reduce chemical waste by spraying only on nutrient deficient crops.",
        },
        {
          title: "Fast Airborne",
          icon: "/icons/aria-tani/drone-spraying/fast-airbone.svg",
          iconHover: "/icons/aria-tani/drone-spraying/fast-airbone-hover.svg",
          description:
            "No pre-flight calibrations necessary. Get airborne in under 2 minutes.",
        },
        {
          title: "Locate & Treat Plants",
          icon: "/icons/aria-tani/drone-spraying/locate-treat-plants.svg",
          iconHover:
            "/icons/aria-tani/drone-spraying/locate-treat-plants-hover.svg",
          description:
            "Detect nutrient deficient plants and take action to prevent crop failure.",
        },
        {
          title: "Increased Flight Safety",
          icon: "/icons/aria-tani/drone-spraying/increased-flight-safety.svg",
          iconHover:
            "/icons/aria-tani/drone-spraying/increased-flight-safety-hover.svg",
          description:
            "Our drones can safely avoid people, animals, utility poles, vehicles, and other drones.",
        },
        {
          title: "Automated Data",
          icon: "/icons/aria-tani/drone-spraying/automated-data.svg",
          iconHover: "/icons/aria-tani/drone-spraying/automated-data-hover.svg",
          description: "Our drones can complete pre-programmed jobs in tandem.",
        },
      ],
    },
    outputExample: {
      title: "Drone Spraying Use Case: Spot Spraying",
      description: "",
      outputItemExamples: [
        {
          image: "/icons/aria-tani/drone-spraying/aerial-photo.svg",
          title: "Aerial Photo",
          description: "Detect field map, nutrient deficiencies and pest",
        },
        {
          image: "/icons/aria-tani/drone-spraying/flight-mission-planning.svg",
          title: "Flight Mission Planning",
          description: "Create flight plan based on Aerial Photo insight",
        },
        {
          image: "/icons/aria-tani/drone-spraying/plant-analysis.svg",
          title: "Plant Analysis",
          description: "Plot specific crops that need fertilizing or pesticide",
        },
        {
          image: "/icons/aria-tani/drone-spraying/flight-implementation.svg",
          title: "Flight Implementation",
          description: "Automatic drone spraying on critical crops",
        },
      ],
      outputImageExamples: [],
      bannerExample: {
        type: "",
        alt: "",
        title: "",
        image: "",
      },
    },
    servicePreview: {
      title: "Drone Spraying Flight Report",
      backgroundImage:
        "/background-image/aria-tani/drone-spraying/drone-spraying-preview-bg.png",
      image:
        "/background-image/aria-tani/drone-spraying/drone-spraying-preview.png",
    },
  },
  "digital-agronomist": {
    meta: {
      title: "Digital Agronomist",
      description: "Measure and monitoring your plant health with our expert.",
      keyword:
        "digital agronomist, agronomis, agronomist, kesehatan tanaman, tanaman sehat, gagal panen",
    },
    title: {
      row1: "Digital",
      row2: "Agronomist",
    },
    banner:
      "/background-image/aria-tani/digital-agronomist/digital-agronomist-banner.png",
    description: {
      row1: "Measure and predict levels of macronutrients (N, P, K) using Agronomy,",
      row2: "Drone and Satellite Imagery, and Machine Learning.",
    },
    features: {
      title: "Digital Agronomist Features",
      description: "Explore our services",
      services: [
        {
          title: "Machine Learning Model",
          icon: "/icons/aria-tani/digital-agronomist/machine-learning.svg",
          iconHover:
            "/icons/aria-tani/digital-agronomist/machine-learning-hover.svg",
          description:
            "Machine learning model with insights from multispectral camera data, satellite data, and agronomy. Measure and predict levels of macronutrients (N, P, K), learn from powerful predictions.",
        },
        {
          title: "Tree Counting & Mapping",
          icon: "/icons/aria-tani/digital-agronomist/tree-counting.svg",
          iconHover:
            "/icons/aria-tani/digital-agronomist/tree-counting-hover.svg",
          description:
            "Gain insights through plant health mapping and early disease detection.",
        },
        {
          title: "Multispectral Camera",
          icon: "/icons/aria-tani/digital-agronomist/multispectral-camera.svg",
          iconHover:
            "/icons/aria-tani/digital-agronomist/multispectral-camera-hover.svg",
          description:
            "Classify areas with sick plants through multispectral aerial photos.",
        },
        {
          title: "Satellite Imagery",
          icon: "/icons/aria-tani/digital-agronomist/satellite-imagery.svg",
          iconHover:
            "/icons/aria-tani/digital-agronomist/satellite-imagery-hover.svg",
          description:
            "View nutrition maps from satellite imagery. Take action to treat sick plants.",
        },
        {
          title: "Locate & Treat Plants",
          icon: "/icons/aria-tani/area-mapping/treat-plants.svg",
          iconHover: "/icons/aria-tani/area-mapping/treat-plants-hover.svg",
          description:
            "Detect nutrient deficient plants and take action to prevent crop failure.",
        },
        {
          title: "Predict Nutrient Content",
          icon: "/icons/aria-tani/digital-agronomist/predict-nutrient-content.svg",
          iconHover:
            "/icons/aria-tani/digital-agronomist/predict-nutrient-content-hover.svg",
          description:
            "Measure and predict nutrient content through satellite imagery and machine learning model.",
        },
        {
          title: "Output Report",
          icon: "/icons/aria-tani/digital-agronomist/output-report.svg",
          iconHover:
            "/icons/aria-tani/digital-agronomist/output-report-hover.svg",
          description:
            "Detailed output report with digestible data insights and actionable item recommendations.",
        },
      ],
    },
    outputExample: {
      title: "Nutrition Map Example",
      description: "",
      outputItemExamples: [],
      outputImageExamples: [
        {
          alt: "nutrition-example",
          image:
            "/background-image/aria-tani/digital-agronomist/nutrition-example.png",
        },
      ],
      bannerExample: {
        type: "",
        alt: "",
        title: "",
        image: "",
      },
    },
    servicePreview: {
      title: "Nutrition Detection on ARIA Tani App",
      backgroundImage:
        "/background-image/aria-tani/digital-agronomist/agro-preview-bg.png",
      image: "/background-image/aria-tani/digital-agronomist/agro-preview.png",
    },
  },
};
