/** @format */

import Head from "next/head";
import AboutHero from "./component/about-hero";
import AriaValue from "./component/aria-value";
import MissionValue from "./component/mission-value";
import OurFounders from "./component/our-founders";

const AboutModule = () => {
  return (
    <>
      <Head>
        <title>About Us | Aria</title>
        <meta
          name="description"
          content="Aria is an agritech company that offers IoT (Internet of Things), mechanisation, and digitalization with a mission to transform Indonesian Agriculture practices to be self sustainable, and securing food autonomy and security."
        />
        <meta
          name="keyword"
          content="Mining, Foresty, agriculture, Aria Indonesia, Aria Agriculture, Internet of Things, mechanisation in agriculture, food security, Indonesian agriculture."
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <AboutHero />
      <MissionValue />
      <AriaValue />
      <OurFounders />
    </>
  );
};

export default AboutModule;
