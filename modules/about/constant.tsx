/** @format */

export const _MISSION_VALUE_ = [
  {
    image: "/logo-asset/about-aria-logo.svg",
    title: "About ARIA",
    description:
      "Aria offers drone technology, IoT (Internet of Things), and big data for the transformation of Indonesian Agriculture. Our services make farming more cost efficient and convenient.",
  },
  {
    image: "/logo-asset/our-mission-logo.svg",
    title: "Our Mission",
    description:
      "Our focus is to promote careers in agriculture, decent work, gender equality, reduce water consumption, reduce climate pollution and provide the means for precision and efficiency in Agriculture.",
  },
];

export const _ARIA_VALUE_ = [
  {
    image: "/logo-asset/commitment-logo.png",
    title: "Commitment",
  },
  {
    image: "/logo-asset/ethics-logo.png",
    title: "Ethics",
  },
  {
    image: "/logo-asset/responsibility-logo.png",
    title: "Responsibility",
  },
  {
    image: "/logo-asset/initiative-logo.png",
    title: "Initiative",
  },
  {
    image: "/logo-asset/agile-logo.png",
    title: "Agile",
  },
];

export const _OUR_FOUNDERS_ = [
  {
    image: "/image-founders/william-sjaichudin.webp",
    title: "William Sjaichudin",
    subTitle: "Chief Executive Officer",
    description:
      "William is also the COO Sahabat Trading Company for 8+ years with revenue over IDR 1T and 16 outlet across Indonesia with over 500 employee also 2nd biggest manufacturing Tapioca producing over 300,000 tons",
  },
  {
    image: "/image-founders/yosa-rosario.webp",
    title: "Yosa Rosario",
    subTitle: "Chief Innovation Officer",
    description:
      "Yosa is also the CEO of NPC Laboratorium Indonesia. He is an Electrical Engineering Graduate and a Drone Specialist since 2014. Yosa developed and manufactured drones for Indonesia’s Ministry of Defense and Agriculture and Mining sectors. ",
  },
  {
    image: "/image-founders/arden-lim.webp",
    title: "Arden Lim",
    subTitle: "Chief Strategy Officer",
    description:
      "Arden is also the COO of Markindo Rekateknik for 8+ years, an industrial automation firm providing coding,  IoT, and packaging solutions for MNCs (CCA, PMI, Unilever). ",
  },
];
