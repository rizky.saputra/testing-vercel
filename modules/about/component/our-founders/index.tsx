/** @format */
import Image from "next/image";
import { _OUR_FOUNDERS_ } from "modules/about/constant";
import { imageLoader } from "common/utils";

const OurFounders = () => {
  return (
    <div className="bg-[#f6f6f6] w-full">
      <div className="px-6 sm:px-[100px] [@media(min-width:1700px)]:px-[300px] lg:px-[120px] h-full items-center">
        <h1 className="text-[14px] lg:text-[36px] text-[#255856] font-semibold py-[24px]">
          Our Founders
        </h1>

        <div className="grid gap-y-6 pb-[40px] lg:grid-cols-3 lg:gap-6">
          {_OUR_FOUNDERS_.map((founders, index) => {
            return (
              <div key={index}>
                <div
                  className="group h-[322px] lg:h-[380px] rounded-[20px] group shadow-[0_3px_22px_rgba(149,175,199,0.2)]"
                  key={index}
                >
                  <div className="group-hover:hidden h-full bg-white rounded-xl">
                    <div className="flex justify-center items-center pt-[50px]">
                      <Image
                        loader={imageLoader}
                        alt={founders.title}
                        src={founders.image}
                        width={0}
                        height={0}
                        className="w-[150px] h-[150px] lg:w-[170px] lg:h-[170px]"
                        priority
                      />
                    </div>
                    <div className="flex justify-center items-center py-[4px] lg:py-[8px]">
                      <div className="text-center pt-[25px] font-semibold text-[#404040]">
                        <h1 className="text-[18px] lg:text-[22px]">
                          {founders.title}
                        </h1>
                      </div>
                    </div>
                    <div className="flex justify-center items-center py-[6px] lg:py-[10px]">
                      <div>
                        <span className="text-[14px] lg:text-[16px] text-[#808080]">
                          {founders.subTitle}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="hidden group-hover:flex w-full h-full relative z-10">
                    <div className="bg-[#255855] h-full w-full absolute top-0 z-[-1] card-masking rounded-[24px]" />
                    <div className="bg-[#255855] brightness-[1.15] h-full w-full absolute top-0 z-[-20] rounded-[24px]" />
                    <div className="flex flex-col p-7 2xl:p-8">
                      <div className="mb-2">
                        <p className="text-[22px] font-semibold text-white">
                          {founders.title}
                        </p>
                      </div>
                      <div className="flex justify-end">
                        <p className="text-[14px] lg:text-[16px] text-white">
                          {founders.description}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default OurFounders;
