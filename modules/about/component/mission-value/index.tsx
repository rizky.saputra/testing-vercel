/** @format */

import Image from "next/image";
import { _MISSION_VALUE_ } from "modules/about/constant";
import { imageLoader } from "common/utils";

const MissionValue = () => {
  return (
    <div className="py-[20px] px-6 sm:px-[100px] [@media(min-width:1700px)]:px-[300px] lg:px-[120px] lg:py-[50px] h-full w-full items-center overflow-auto">
      <h1 className="text-[#255856] text-[14px] lg:text-[36px]">
        Who we are
        <span className=" block text-[12px] lg:text-[36px] text-[#404040]">
          Our mission in the agriculture sector
        </span>
      </h1>

      <div className="grid lg:grid-cols-2 gap-y-4 lg:gap-4 pt-[10px] ">
        {_MISSION_VALUE_.map((misi, index) => {
          return (
            <div
              className="h-full bg-white rounded-xl pl-[10px] pt-[6px] shadow-[0_3px_22px_rgba(149,175,199,0.2)]"
              key={index}
            >
              <div>
                <Image
                  loader={imageLoader}
                  alt={misi.title}
                  src={misi.image}
                  width={0}
                  height={0}
                  className="w-[60px] h-[60px] lg:w-[100px] lg:h-[100px]"
                  priority
                />
                <div className="px-[20px] pb-[20px]">
                  <h1 className=" text-[12px] lg:text-[22px] font-semibold">
                    {misi.title}
                    <div className=" pt-[2px] text-[12px] lg:text-[16px] font-normal text-[#808080]">
                      <span>{misi.description}</span>
                    </div>
                  </h1>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default MissionValue;
