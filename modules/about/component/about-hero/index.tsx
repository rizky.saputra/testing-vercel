/** @format */
import { imageLoader } from "common/utils";
import Image from "next/image";

const AboutHero = () => {
  return (
    <>
      <DesktopAboutHero />
      <MobileAboutHero />
    </>
  );
};

export default AboutHero;

const MobileAboutHero = () => {
  return (
    <div className="sm:hidden relative w-full h-[194px]">
      <div className=" absolute w-full h-[194px] z-[-10]">
        <Image
          loader={imageLoader}
          alt={"about-banner"}
          src={"/background-image/about/about-banner.webp"}
          width={360}
          height={194}
          className="object-fit absolute w-[100%] h-[100%]"
          priority
        />
      </div>
      <div className="px-[30px] py-[20px] pt-[50px] h-full items-center">
        <div className="px-[12px]">
          <h1 className="text-[30px] leading-[2rem] text-white font-semibold ">
            About Us
            <span className="text-[#07A9A0] block">Culture & Mission</span>
          </h1>
        </div>
        <span className="text-[12px] text-white">
          Explore what’s behind our AgriTech solutions and
          <p className="block">
            our drive to transform Indonesian Agriculture.
          </p>
        </span>
      </div>
    </div>
  );
};

const DesktopAboutHero = () => {
  return (
    <div className="hidden sm:block">
      <div className="relative h-[600px] 2xl:h-[800px]">
        <div className=" absolute w-full h-[600px] 2xl:h-[800px] z-[-1]">
          <Image
            loader={imageLoader}
            alt={"about-banner"}
            src={"/background-image/about/about-banner.webp"}
            width={940}
            height={311}
            className="object-fit absolute w-[100%] h-[100%]"
            priority
          />
        </div>
        <div className="flex sm:px-[100px] [@media(min-width:1700px)]:px-[300px] [@media(min-width:1800px)]:px-[200px] [@media(min-width:2000px)]:px-[300px] h-full items-center">
          <div>
            <h1 className="text-[64px] leading-[5rem] text-white font-semibold ">
              About Aria
              <span className="text-[#07A9A0] block">Culture & Mission</span>
            </h1>
            <span className="text-white text-[22px] mt-5">
              Explore what’s behind our AgriTech solutions and our drive to
              transform
              <p className="block">Indonesian Agriculture.</p>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};
