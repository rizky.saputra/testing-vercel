/** @format */
import { imageLoader } from "common/utils";
import { _ARIA_VALUE_ } from "modules/about/constant";
import Image from "next/image";

const AriaValue = () => {
  return (
    <div className="h-full items-center">
      <div className="absolute w-full h-[620px] lg:h-[380px] z-[-1]">
        <Image
          loader={imageLoader}
          alt="title"
          src="/background-image/about/aria-value-banner.webp"
          fill
          priority
        />
      </div>
      <div className="pt-[25px] pb-[12px] lg:pt-8">
        <p className="text-[12px] lg:text-[34px] text-center text-white">
          We have five values in our environment that
          <span className="block">
            encourages us to achieve goals, keeps our passion
            <span className="block">
              alive, and guide our daily effort to overall impact.
            </span>
          </span>
        </p>
      </div>
      <div className="gap-4 gap-y-4 lg:gap-8 lg:gap-y-8 mt-2 mb-8 flex flex-wrap justify-center sm:px-[100px] [@media(min-width:1700px)]:px-[300px]">
        {_ARIA_VALUE_.map((value, index) => {
          return (
            <div
              className="basis-36 lg:basis-[360px] lg:h-[256px] bg-white rounded-xl shadow-lg"
              key={index}
            >
              <div className="flex justify-center items-center pt-[28px] lg:pt-14">
                <Image
                  loader={imageLoader}
                  alt={value.title}
                  src={value.image}
                  width={0}
                  height={0}
                  className="w-[60px] h-[60px] lg:w-[90px] lg:h-[90px]"
                  priority
                />
              </div>
              <div className="text-center font-semibold text-[#404040] pb-4 lg:pt-[10px]">
                <span className="text-[12px] lg:text-[25px]">
                  {value.title}
                </span>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default AriaValue;
