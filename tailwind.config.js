/** @type {import('tailwindcss').Config} */
const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  mode: "jit",
  content: [
    "./common/components/**/*.{js,ts,jsx,tsx}",
    "./modules/**/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Poppins", ...defaultTheme.fontFamily.sans],
        barlow: ["Barlow", "sans-serif"],
      },
      dropShadow: {
        aria: "0px 3px 22px rgba(149, 175, 199, 0.2)",
      },
    },
  },
  plugins: [],
};
