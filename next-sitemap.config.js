const generatePolicies = () => {
  if (process.env.ENVIRONMENT === "production") {
    return [
      {
        userAgent: "*",
        allow: "/",
        disallow: ["/app"],
        outDir: "/out",
      },
    ];
  }
  return [
    {
      userAgent: "*",
      disallow: ["/"],
      outDir: "/out",
    },
  ];
};
module.exports = {
  siteUrl: process.env.NEXT_PUBLIC_HOST,
  generateRobotsTxt: true,
  robotsTxtOptions: {
    policies: generatePolicies(),
    additionalSitemaps: [], // add wordpress sitemap path here
  },
};
