import { _ALL_SERVICE_ } from "modules/aria-agriculture/constant";
import ServiceModule from "modules/aria-agriculture/service";
import { GetStaticPaths, GetStaticProps } from "next";
import Head from "next/head";

const Service = (props: any) => {
  return (
    <>
      <Head>
        <title>{props.data.meta.title}</title>
        <meta name="description" content={props.data.meta.description} />
        <meta name="keyword" content={props.data.meta.keyword} />
      </Head>
      <ServiceModule {...props} />;
    </>
  );
};

export default Service;

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const path = Array.isArray(params!.slug) ? "" : params!.slug;
  return {
    props: {
      data: _ALL_SERVICE_[path || ""],
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const servicePath = Object.keys(_ALL_SERVICE_);
  return {
    paths: servicePath.map((path) => `/aria-agriculture/${path}`) || [],
    fallback: false,
  };
};
