import AriaAgricultureModule from "modules/aria-agriculture";

const AriaAgriculture = () => {
  return <AriaAgricultureModule />;
};

export default AriaAgriculture;
