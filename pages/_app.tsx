import type { AppProps } from "next/app";
import Layout from "common/components/layout";
import "../styles/globals.css";
import "pure-react-carousel/dist/react-carousel.es.css";

function App(props: AppProps) {
  return <Layout {...props} />;
}

export default App;
