import Cookies from "js-cookie";
import HomeModule from "modules/home";
import { useRouter } from "next/router";
import { useEffect } from "react";

const Home = () => {
  const router = useRouter();
  const loaded = Cookies.get("loaded");

  console.log(process.env.ENVIRONMENT, "Where Am I");

  useEffect(() => {
    const lastPath = window.location.pathname;
    const search = window.location.search;
    const uri = lastPath + search;

    if (router && loaded !== uri) {
      if (window.location.pathname !== "/") {
        Cookies.set("loaded", uri);
        router.push(uri).catch((err) => {
          // console.log("failure", err);
        });
      }
    }
    return () => {
      Cookies.remove("loaded");
    };
  }, [router, loaded]);

  return <HomeModule />;
};

export default Home;
