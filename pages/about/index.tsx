import AboutModule from "modules/about";

const About = () => {
  return <AboutModule />;
};

export default About;
