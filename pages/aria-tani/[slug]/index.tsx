import { _ALL_SERVICE_ } from "modules/aria-tani/constant";
import ServiceModule from "modules/aria-tani/service";
import { GetStaticPaths, GetStaticProps } from "next";

const Service = (props: any) => {
  return <ServiceModule {...props} />;
};

export default Service;

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const path = Array.isArray(params!.slug) ? "" : params!.slug;
  return {
    props: {
      data: _ALL_SERVICE_[path || ""],
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const servicePath = Object.keys(_ALL_SERVICE_);
  return {
    paths: servicePath.map((path) => `/aria-tani/${path}`) || [],
    fallback: false,
  };
};
