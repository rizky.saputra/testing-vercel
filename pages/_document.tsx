import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html lang="en" className="scroll-smooth">
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Barlow:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet"
        />
        {/* <meta
          property="og:image"
          content={`${process.env.NEXT_PUBLIC_HOST}/og-logo-300.png`}
        />
        <meta property="og:image:width" content="300" />
        <meta property="og:image:height" content="300" /> */}
        {/* howuku and google Analytics */}
        {process.env.ENVIRONMENT === "production" && (
          <>
            <script
              async
              src={`https://www.googletagmanager.com/gtag/js?id=${process.env.GA_TRACKING_ID}`}
            />
            <script
              dangerouslySetInnerHTML={{
                __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', '${process.env.GA_TRACKING_ID}', {
                page_path: window.location.pathname,
              });`,
              }}
            />

            <script
              dangerouslySetInnerHTML={{
                __html: `
                  (function(t,r,a,c,k){
                    c=['track','identify','converted'],t.o=t._init||{},
                    c.map(function(n){return t.o[n]=t.o[n]||function(){(t.o[n].q=t.o[n].q||[]).push(arguments);};}),t._init=t.o,
                    k=r.createElement("script"),k.type="text/javascript",k.async=true,k.src="https://cdn.howuku.com/js/track.js",k.setAttribute("key",a),
                    r.getElementsByTagName("head")[0].appendChild(k);
                  })(window, document, "34MV9Wnqd0KjpBXrGNLpJw");`,
              }}
            />
          </>
        )}
        <meta
          property="og:image"
          content={`${process.env.NEXT_PUBLIC_HOST}/og-logo.png`}
        />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="630" />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
