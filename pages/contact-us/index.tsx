import ContactModule from "modules/contact-us";

const Contact = () => {
  return <ContactModule />;
};

export default Contact;
